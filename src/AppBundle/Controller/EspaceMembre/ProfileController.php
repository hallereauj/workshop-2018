<?php

namespace AppBundle\Controller\EspaceMembre;

use AppBundle\Entity\Wine;
use AppBundle\Service\BoutiqueService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class ProfileController extends Controller
{
	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @param BoutiqueService $boutiqueService
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @Route("/mon-profil", name="mon-profil")
	 */
    public function indexAction(Request $request, EntityManagerInterface $em)
    {
    	$user = $this->getUser();

    	$commandes = $em->getRepository("AppBundle:Boutique\Commande")->findBy([
            'user'=>$user,
            'isValide'=>true
        ]);


    	return $this->render("espace-membre/profil/index.html.twig", [
    		'user'=>$user,
    		'commandes'=>$commandes
    	]);
    }
}
