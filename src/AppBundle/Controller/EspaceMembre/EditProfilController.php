<?php

namespace AppBundle\Controller\EspaceMembre;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Form\EditUserType;

class EditProfilController extends Controller
{
	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @Route("/editer", name="editer-profil")
	 */
    public function indexAction(Request $request, EntityManagerInterface $em)
    {
    	$user = $this->getUser();

    	$form = $this->createForm(EditUserType::class, $user);

    	$form->handleRequest($request);

    	if($form->isSubmitted()&&$form->isValid()){
    		$em->persist($user);
    		$em->flush();
    	}

    	return $this->render("espace-membre/profil/edit.html.twig", [
    		'user'=>$user,
    		'form'=>$form->createView(),
    	]);
    }
}
