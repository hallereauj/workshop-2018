<?php
/**
 * Created by PhpStorm.
 * User: benjamin
 * Date: 20/09/2018
 * Time: 10:02
 */

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package AppBundle\Controller\Admin
 * @Route("/")
 */
class AdminController extends AbstractController
{
	/**
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @Route("/", name="admin_index", methods={"GET"})
	 */
	public function indexAction(Request $request)
	{
		return $this->render('admin/index.html.twig');
	}
}