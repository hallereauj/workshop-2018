<?php
/**
 * Created by PhpStorm.
 * User: benjamin
 * Date: 21/09/2018
 * Time: 09:54
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Boutique\Commande;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package AppBundle\Controller\Admin
 * @Route("/users")
 */
class UserController extends Controller
{
	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @Route("/", name="admin_users_index", methods={"GET"})
	 */
	public function indexAction(Request $request, EntityManagerInterface $em)
	{
		$users = $em->getRepository(User::class)->findAll();
		return $this->render('admin/user/index.html.twig', [
			'users' => $users
		]);
	}

	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @param User $user
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @Route("/{id}", name="admin_user_voir", methods={"GET"})
	 */
	public function voirAction(Request $request, EntityManagerInterface $em, User $user)
	{
		$commandes = $em->getRepository(Commande::class)->findBy(['user' => $user]);
		return $this->render('admin/user/view.html.twig', [
			'user' => $user,
			'commandes' => $commandes
		]);
	}
}