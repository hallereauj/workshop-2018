<?php
/**
 * Created by PhpStorm.
 * User: benjamin
 * Date: 20/09/2018
 * Time: 09:49
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Boutique\Commande;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 * @package AppBundle\Controller\Admin
 * @Route("/commandes")
 */
class OrderController extends AbstractController
{
	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @Route("/", name="admin_order_index", methods={"GET"})
	 */
	public function indexAction(Request $request, EntityManagerInterface $em)
	{
		$listeCommandes = $em->getRepository(Commande::class)->findAll();

		return $this->render('admin/order/index.html.twig', [
			'listeCommandes' => $listeCommandes
		]);
	}
}