<?php
/**
 * Created by PhpStorm.
 * User: benjamin
 * Date: 20/09/2018
 * Time: 09:49
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Boutique\Taxe;
use AppBundle\Entity\Wine;
use AppBundle\Form\ProductType;
use AppBundle\Utils\GLService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 * @package AppBundle\Controller\Admin
 * @Route("/product")
 */
class ProductController extends AbstractController
{
	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @Route("/", name="admin_wine_index", methods={"GET"})
	 */
	public function indexAction(Request $request, EntityManagerInterface $em)
	{
		$listeProduits = $em->getRepository(Wine::class)->findAll();

		return $this->render('admin/product/index.html.twig', [
			'listeProduits' => $listeProduits
		]);
	}

	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @param GLService $glService
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 * @Route("/add", name="admin_wine_add", methods={"GET", "POST"})
	 */
	public function addAction(Request $request, EntityManagerInterface $em, GLService $glService)
	{
		$monProduit = new Wine();
		$form = $this->createForm(ProductType::class, $monProduit);
		$form->add('save', SubmitType::class);
		$form->add('saveAndStay', SubmitType::class);
		$form->add('thumbnail', FileType::class);

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$maTaxe = $em->getRepository(Taxe::class)->find(1);
			$monProduit->setTaxe($maTaxe);
			$monProduit->setReference($glService->genererChaineAleatoire(5, "ABCDEFGHJKLMNPQRSTUVWXYZ23456789"));
			$em->persist($monProduit);
			$em->flush();
			$this->addFlash( 'notification-admin', ['statut' => 'success', 'contenu' => '<span class="glyphicon glyphicon-ok"></span> Vin ajouté avec <strong>succès</strong>.']);

			return $this->redirectToRoute('admin_wine_index');
		}

		return $this->render('admin/product/add.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @param Wine $wine
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 * @Route("/{id}/edit", name="admin_wine_edit", methods={"GET", "POST"})
	 */
	public function editAction(Request $request, EntityManagerInterface $em, Wine $wine)
	{
		$form = $this->createForm(ProductType::class, $wine);
		$form->add('save', SubmitType::class);
		$form->add('saveAndStay', SubmitType::class);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em->flush();
			$this->addFlash( 'notification-admin', ['statut' => 'success', 'contenu' => '<span class="glyphicon glyphicon-ok"></span> Vin modifié avec <strong>succès</strong>.']);

			return $this->redirectToRoute('admin_wine_index');
		}

		return $this->render('admin/product/edit.html.twig', [
			'form' => $form->createView(),
			'wine' => $wine
		]);
	}

	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @param Wine $wine
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 * @Route("/{id}/supprimer", name="admin_wine_delete", requirements={"id"="\d+"}, options={"expose"=true}, methods={"GET", "POST"})
	 */
	public function deleteAction(Request $request, EntityManagerInterface $em, Wine $wine)
	{
		// on crée un formulaire vide, qui ne contiendra que le champ CSRF .. Cela permet de protéger la suppression contre cette faille
		$form = $this->createFormBuilder()->getForm();

		$form->handleRequest($request);
		// si le formulaire est posté et valide
		if ($form->isSubmitted() && $form->isValid()) {

			// on fait un hard delete
			// $em->remove($wine);
			// on fait un soft delete
			$wine->setDeletedAt(new \DateTime());
			$em->flush();

			$this->addFlash( 'notification-admin', ['statut' => 'success', 'contenu' => '<span class="glyphicon glyphicon-ok"></span> Vin supprimé avec <strong>succès</strong>.']);

			// on redirige
			return $this->redirectToRoute('admin_wine_index');
		}

		return $this->render('admin/product/delete.html.twig', [
			'form' => $form->createView(),
			'wine' => $wine,
		]);
	}

}