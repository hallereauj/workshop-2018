<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Wine;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class HomepageController extends Controller
{
    /**
     * @param Request $request
	 * @param EntityManagerInterface $em
	 * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request, EntityManagerInterface $em)
    {

        $data = [];
        $form = $this->createFormBuilder($data)
            ->add('name', TextType::class, ['required' => false])
            ->add('minPrice', TextType::class, ['required' => false])
            ->add('maxPrice', TextType::class, ['required' => false])
            ->add('variete', ChoiceType::class, [
                'required' => false,
                'choices'=>[
                    'Aucune' => '',
                    'Rosé' => 'rose',
                    'Blanc' => 'blanc',
                    'Rouge' => 'rouge',
                    'Effervescent' => 'effervescent'
                ]
            ])
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted()&& $form->isValid()){
            $name = $form->get('name')->getData();
            $minPrice = $form->get('minPrice')->getData();
            $maxPrice = $form->get('maxPrice')->getData();
            $variete = $form->get('variete')->getData();

            return $this->redirectToRoute('fo_search', [
                'name' => $name,
                'minPrice' => $minPrice,
                'maxPrice' => $maxPrice,
                'variete' => $variete,
            ]);
        }

        // On récupère l'intégralité des objets 'wine'
        $wines = $em->getRepository(Wine::class)->findBy([], [], 6);

        // On renvoie la vue désirée, avec son path en premier argument, les variables que l'on souhaite lui transmettre en second
        return $this->render('homepage/index.html.twig', [
            'wines' => $wines,
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
        ]);
    }

    /**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @Route("/search", name="fo_search")
	 */
    public function searchAction(Request $request, EntityManagerInterface $em){

        $name = $request->get('name');
        $minPrice = $request->get('minPrice');
        $maxPrice = $request->get('maxPrice');
        $variete = $request->get('variete');

        $wines = $em->getRepository('AppBundle:Wine')->getWineByArguments($name,$minPrice,$maxPrice,$variete);

        return $this->render('homepage/search.html.twig', [
    		'wines' => $wines,
	    ]);
    }
}
