<?php
/**
 * Created by PhpStorm.
 * User: benjamin
 * Date: 20/09/2018
 * Time: 20:36
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Boutique\Commande;
use AppBundle\Entity\Boutique\Facture;
use AppBundle\Entity\Boutique\LigneCommande;
use AppBundle\Entity\Boutique\Statut;
use AppBundle\Entity\User;
use AppBundle\Service\BoutiqueService;
use AppBundle\Utils\GLService;
use Doctrine\ORM\EntityManagerInterface;
use Stripe\Charge;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrderController
 * @package AppBundle\Controller
 * @Route("/paiement")
 */
class OrderController extends Controller
{
	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @param SessionInterface $session
	 * @param BoutiqueService $boutiqueService
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @Route("/", name="paiement")
	 */
	public function indexAction(Request $request, EntityManagerInterface $em, SessionInterface $session, BoutiqueService $boutiqueService)
	{
		$monUser = $this->getUser();
		if (is_null($session->get('commande'))) {
			$commandeId = $boutiqueService->creerCommande($monUser);
			$session->set('commande', $commandeId);
		}

		$commandeId = $session->get('commande');
		$maCommande = $em->getRepository(Commande::class)->find($commandeId);
		$amout = $maCommande->getTotalPayeTtc() * 100;

		return $this->render('order/index.html.twig', [
			'amount' => $amout,
			'maCommande' => $maCommande,
			'monUser' => $monUser
		]);
	}

	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @param SessionInterface $session
	 * @param GLService $glService
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
	 * @Route("/order-checkout", name="order_checkout")
	 */
	public function orderCheckoutAction(Request $request, EntityManagerInterface $em, SessionInterface $session, GLService $glService)
	{
		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here: https://dashboard.stripe.com/account/apikeys
		$key = $this->getParameter('api_stripe_key');
		Stripe::setApiKey($key);

		// Token is created using Checkout or Elements!
		// Get the payment token ID submitted by the form:
		$token = $request->get('stripeToken');
		$commandeId = $request->get('commande');
		$userId = $request->get('user');
		$maCommande = $em->getRepository(Commande::class)->find($commandeId);
		$monUser = $em->getRepository(User::class)->find($userId);

		if (!$maCommande)
			return $this->createNotFoundException('Commande introuvable');

		if (!$monUser)
			return $this->createNotFoundException('Utilisateur introuvable');

		$charge = Charge::create([
			'amount' => $maCommande->getTotalPayeTtc()*100,
			'currency' => 'usd',
			'description' => 'Example charge',
			'source' => $token,
		]);

        // 0- INITIALISATION

        //////////////////////////////////////////////////////////////////////////////////
        // 2- on genere la facture
        //////////////////////////////////////////////////////////////////////////////////
        // 2.1- on cree l'objet
        $maFacture = $em->getRepository(Facture::class)->findOneBy(['commande'=>$maCommande]);
        // on vérifie
        if (!$maFacture) {
            $maFacture = new Facture();
            $maFacture->setCommande($maCommande);
            $maFacture->setNumero(uniqid());
            $maFacture->setTotalPayeTtc($maCommande->getTotalPayeTtc());
            $maFacture->setTotalPayeHt($maCommande->getTotalPayeHt());
            $maFacture->setTotalProduitsTtc($maCommande->getTotalProduitsTtc());
            $maFacture->setTotalProduitsHt($maCommande->getTotalProduitsHt());
            $em->persist($maFacture);

            // /!\ on est obligé de flusher pour crer l'ID de la facture qui va être insérer dans commande et lignecommande
            $em->flush();
        }
        else {
            throw new \LogicException("Une facture existe déjà. Merci de contacter le support !");
        }

        //////////////////////////////////////////////////////////////////////////////////
        // 3- on modifie les paramètres de la commande
        //////////////////////////////////////////////////////////////////////////////////
        // 3.1- la commande
		$monStatut = $em->getRepository(Statut::class)->find(2);
        $maCommande->setStatut($monStatut);
        // $maCommande->setReponseAcquereur($maReponseAcquereur); // pas de réponse si chèque
        // $maCommande->setPaiement(Paiement::CHEQUE); // pas necessairement par CHEQUE (peut-être du à un retour de banque defectueux)
        $maCommande->setTotalPayeReel($maCommande->getTotalPayeTtc()); // on suppose que le paiement par cheque est du bon montant
        // $maCommande->setFacture($maFacture);
        $maCommande->setNumeroFacture($maFacture->getNumero());
        $maCommande->setFactureCreatedAt(new \DateTime());
        $maCommande->setIsValide(true);
        $maCommande->setUpdatedAt(new \DateTime());

        // 3.2- ...puis les lignes de commande
        $listeLignesCommande = $em->getRepository(LigneCommande::class)->findBy(['commande'=>$maCommande]);
        foreach($listeLignesCommande as $uneLigneCommande ) {
            $uneLigneCommande->setFactureId($maFacture->getId());
        }

        // On vide la session
        $em->flush();
        $session->clear();

        // Envoi d'email
        $from = [$this->getParameter('app_email_sender') => $this->getParameter('app_email_sender_name')];
        $to = [$monUser->getEmail() => $monUser->getFirstname()];
        $bcc = [];
        $sujet = "Votre commande a été validée";
        $template = "confirmation_commande";
        $params = [
        	"maCommande" => $maCommande
        ];
        $glService->sendEmail($from, $to, $bcc, $sujet, $template, $params);

        $this->addFlash('notification-site', ['statut' => 'success', 'contenu' => 'Votre commande a été validée']);
        return $this->redirectToRoute('homepage');

	}
}