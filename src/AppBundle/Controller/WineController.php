<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Wine;
use AppBundle\Service\BoutiqueService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class WineController
 * @package AppBundle\Controller
 * @Route("/vin")
 */
class WineController extends Controller
{
	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @param BoutiqueService $boutiqueService
	 * @param Wine $wine
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @Route("/{id}", name="site_voir_vin", methods={"GET", "POST"})
	 */
    public function indexAction(Request $request, EntityManagerInterface $em, BoutiqueService $boutiqueService, Wine $wine)
    {

        $range = array();
        foreach (range(1,10) as $key => $value) {
            $range[$value] = $value;
        }
    	$suggestions = $em->getRepository(Wine::class)->getSuggestions($wine->getId(), $wine->getVariete());
    	$form = $this->createFormBuilder()
		    ->add('quantite', ChoiceType::class, [
		    	'required' => true,
			    'choices' => $range
		    ])
		    ->add('produit', HiddenType::class, [
		    	'data' => $wine->getId()
		    ])
		    ->getForm()
	    ;
    	$form->handleRequest($request);

    	if ($form->isSubmitted() && $form->isValid()) {
    		$data = $form->getData();
    		$boutiqueService->ajouterLignePanier($data);
    		$this->addFlash('notification-site', ['statut' => 'success', 'contenu' => 'Le produit a été ajouté au panier']);
    		return $this->redirectToRoute('cart');
	    }

        // On renvoie la vue
        return $this->render('details/index.html.twig', [
            'wine' => $wine,
            'suggestions' => $suggestions,
	        'form' => $form->createView()
        ]);
    }



}
