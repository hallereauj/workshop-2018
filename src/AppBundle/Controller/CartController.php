<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Wine;
use AppBundle\Service\BoutiqueService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class CartController extends Controller
{
	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @param BoutiqueService $boutiqueService
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @Route("/panier", name="cart", options={"expose"=true})
	 */
    public function indexAction(Request $request, EntityManagerInterface $em, BoutiqueService $boutiqueService)
    {
		$panier = $boutiqueService->getPanier();
		$listeProduits = [];
		foreach ($panier as $id => $quantite) {
			$unProduit = $em->getRepository(Wine::class)->find($id);
			$prix = $quantite * $unProduit->getPrice();
			$listeProduits[$id]['produit'] = $unProduit;
			$listeProduits[$id]['quantite'] = $quantite;
			$listeProduits[$id]['prix'] = $prix;
		}

		$form = $this->createFormBuilder()->getForm();
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			return $this->redirectToRoute('paiement');
		}

        // On renvoie la vue désirée, avec son path en premier argument, les variables que l'on souhaite lui transmettre en second
        return $this->render('cart/index.html.twig', [
			'listeProduits' => $listeProduits,
	        'form' => $form->createView()
        ]);
    }

	/**
	 * @param Request $request
	 * @param BoutiqueService $boutiqueService
	 * @return Response|\Symfony\Component\Security\Core\Exception\AccessDeniedException
	 * @Route("/{id}/supprimer-ligne-panier", name="supprimer_ligne_panier", options={"expose"=true})
	 */
	public function supprimerLignePanierAction(Request $request, BoutiqueService $boutiqueService)
	{
		if (!$request->isXmlHttpRequest())
			return $this->createAccessDeniedException('You can\'t acces this file');

		$id = $request->get('id');
		$boutiqueService->supprimerLignePanier($id);

		return new Response('ok');
	}
}
