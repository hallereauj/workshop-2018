<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Wine;
use AppBundle\Service\BoutiqueService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class CatalogueController extends Controller
{
	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @Route("/catalogue", name="catalogue")
	 */
	public function indexAction(Request $request, EntityManagerInterface $em)
	{

		$listeVins = $em->getRepository(Wine::class)->findAll();

		$leCatalogue  = $this->get('knp_paginator')->paginate(
			$listeVins,
			$request->query->get('page', 1)/*le numéro de la page à afficher*/,
			1/*nbre d'éléments par page*/
		);

		return $this->render('catalogue/index.html.twig', [
			'catalogue' => $leCatalogue,
		]);

	}
}
