<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Wine
 *
 * @ORM\Table(name="wine")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WineRepository")
 */
class Wine
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Boutique\Taxe")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $taxe;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

        /**
     * @var string
     *
     * @ORM\Column(name="variete", type="string", length=255)
     */
    private $variete;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="reference", type="string", length=255)
	 */
	private $reference;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text")
	 */
	private $description;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="price", type="float")
	 */
	private $price;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 *
	 * @Assert\NotBlank(message="Please, upload the thumbnail as a JPG file.")
	 * @Assert\File(mimeTypes={ "image/jpg", "image/jpeg" })
	 */
	private $thumbnail;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="origin", type="string", length=255)
	 */
	private $origin;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created_at", type="datetime")
	 */
	private $createdAt;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
	 */
	private $deletedAt;

	public function __construct()
	{
		$this->createdAt = new \DateTime();
	}


	/**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Wine
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Wine
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price.
     *
     * @param float $price
     *
     * @return Wine
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set thumbnail.
     *
     * @param string|null $thumbnail
     *
     * @return Wine
     */
    public function setThumbnail($thumbnail = null)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail.
     *
     * @return string|null
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set origin.
     *
     * @param string $origin
     *
     * @return Wine
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * Get origin.
     *
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Wine
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime|null $deletedAt
     *
     * @return Wine
     */
    public function setDeletedAt($deletedAt = null)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime|null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set reference.
     *
     * @param string $reference
     *
     * @return Wine
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference.
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set taxe.
     *
     * @param \AppBundle\Entity\Boutique\Taxe|null $taxe
     *
     * @return Wine
     */
    public function setTaxe(\AppBundle\Entity\Boutique\Taxe $taxe = null)
    {
        $this->taxe = $taxe;

        return $this;
    }

    /**
     * Get taxe.
     *
     * @return \AppBundle\Entity\Boutique\Taxe|null
     */
    public function getTaxe()
    {
        return $this->taxe;
    }

    /**
     * Set variete.
     *
     * @param string $variete
     *
     * @return Wine
     */
    public function setVariete($variete)
    {
        $this->variete = $variete;

        return $this;
    }

    /**
     * Get variete.
     *
     * @return string
     */
    public function getVariete()
    {
        return $this->variete;
    }
}
