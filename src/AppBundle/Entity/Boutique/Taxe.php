<?php

namespace AppBundle\Entity\Boutique;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Taxe
 *
 * @ORM\Table(name="btq_taxe")
 * @ORM\Entity
 */
class Taxe
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="nom", type="string", length=255)
	 */
	private $nom;

	/**
	 * @var string
	 * @Gedmo\Slug(fields={"nom"})
	 * @ORM\Column(name="reference", type="string", length=255,)
	 */

	private $reference;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="taux", type="decimal", precision=10, scale=2)
	 */
	private $taux;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="rang", type="integer")
	 */
	private $rang;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="statut", type="boolean")
	 */
	private $statut;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created_at", type="datetime")
	 */
	private $createAt;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="updated_at", type="datetime", nullable=true)
	 */
	private $updateAt;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
	 */
	private $deletedAt;



	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->taux = 0.00;
		$this->rang = 1;
		$this->statut = true;
		$this->createAt = new \DateTime();
		$this->updateAt = null;
		$this->deletedAt = null;
	}



	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set nom
	 *
	 * @param string $nom
	 *
	 * @return Taxe
	 */
	public function setNom($nom)
	{
		$this->nom = $nom;

		return $this;
	}

	/**
	 * Get nom
	 *
	 * @return string
	 */
	public function getNom()
	{
		return $this->nom;
	}

	/**
	 * Set reference
	 *
	 * @param string $reference
	 *
	 * @return Taxe
	 */
	public function setReference($reference)
	{
		$this->reference = $reference;

		return $this;
	}

	/**
	 * Get reference
	 *
	 * @return string
	 */
	public function getReference()
	{
		return $this->reference;
	}

	/**
	 * Set taux
	 *
	 * @param string $taux
	 *
	 * @return Taxe
	 */
	public function setTaux($taux)
	{
		$this->taux = $taux;

		return $this;
	}

	/**
	 * Get taux
	 *
	 * @return string
	 */
	public function getTaux()
	{
		return $this->taux;
	}

	/**
	 * Set rang
	 *
	 * @param integer $rang
	 *
	 * @return Taxe
	 */
	public function setRang($rang)
	{
		$this->rang = $rang;

		return $this;
	}

	/**
	 * Get rang
	 *
	 * @return integer
	 */
	public function getRang()
	{
		return $this->rang;
	}

	/**
	 * Set statut
	 *
	 * @param boolean $statut
	 *
	 * @return Taxe
	 */
	public function setStatut($statut)
	{
		$this->statut = $statut;

		return $this;
	}

	/**
	 * Get statut
	 *
	 * @return boolean
	 */
	public function getStatut()
	{
		return $this->statut;
	}

	/**
	 * Set createAt
	 *
	 * @param \DateTime $createAt
	 *
	 * @return Taxe
	 */
	public function setCreateAt($createAt)
	{
		$this->createAt = $createAt;

		return $this;
	}

	/**
	 * Get createAt
	 *
	 * @return \DateTime
	 */
	public function getCreateAt()
	{
		return $this->createAt;
	}

	/**
	 * Set updateAt
	 *
	 * @param \DateTime $updateAt
	 *
	 * @return Taxe
	 */
	public function setUpdateAt($updateAt)
	{
		$this->updateAt = $updateAt;

		return $this;
	}

	/**
	 * Get updateAt
	 *
	 * @return \DateTime
	 */
	public function getUpdateAt()
	{
		return $this->updateAt;
	}

	/**
	 * Set deletedAt
	 *
	 * @param \DateTime $deletedAt
	 *
	 * @return Taxe
	 */
	public function setDeletedAt($deletedAt)
	{
		$this->deletedAt = $deletedAt;

		return $this;
	}

	/**
	 * Get deletedAt
	 *
	 * @return \DateTime
	 */
	public function getDeletedAt()
	{
		return $this->deletedAt;
	}
}
