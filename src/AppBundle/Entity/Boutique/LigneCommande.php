<?php

namespace AppBundle\Entity\Boutique;

use Doctrine\ORM\Mapping as ORM;

/**
 * LigneCommande
 *
 * @ORM\Table(name="btq_lignecommande")
 * @ORM\Entity
 */
class LigneCommande
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Boutique\Commande", inversedBy="ligneCommandes")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $commande;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Wine")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $wine;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="facture_id", type="integer", nullable=true)
	 */
	private $factureId;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="produit_nom", type="string", length=255)
	 */
	private $produitNom;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="produit_reference", type="string", length=32, nullable=true)
	 */
	private $produitReference;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="quantite", type="integer")
	 */
	private $quantite;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="prix_unitaire_ttc", type="decimal", precision=17, scale=2)
	 */
	private $prixUnitaireTtc;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="prix_unitaire_ht", type="decimal", precision=17, scale=2)
	 */
	private $prixUnitaireHt;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_prix_ttc", type="decimal", precision=17, scale=2)
	 */
	private $totalPrixTtc;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_prix_ht", type="decimal", precision=17, scale=2)
	 */
	private $totalPrixHt;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="reduction_pourcent", type="decimal", precision=10, scale=2)
	 */
	private $reductionPourcent;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="reduction_montant_ttc", type="decimal", precision=17, scale=2)
	 */
	private $reductionMontantTtc;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="reduction_montant_ht", type="decimal", precision=17, scale=2)
	 */
	private $reductionMontantHt;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="taxe_nom", type="string", length=255)
	 */
	private $taxeNom;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="taxe_taux", type="decimal", precision=10, scale=3)
	 */
	private $taxeTaux;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ecotaxe", type="decimal", precision=17, scale=2)
	 */
	private $ecotaxe;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_expedition_ttc", type="decimal", precision=17, scale=2)
	 */
	private $totalExpeditionTtc;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_expedition_ht", type="decimal", precision=17, scale=2)
	 */
	private $totalExpeditionHt;



	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->quantite = 0;
		$this->prixUnitaireTtc = 0.00;
		$this->prixUnitaireHt = 0.00;
		$this->totalPrixTtc = 0.00;
		$this->totalPrixHt = 0.00;
		$this->reductionPourcent = 0.00;
		$this->reductionMontantTtc = 0.00;
		$this->reductionMontantHt = 0.00;
		$this->taxeTaux = 0.000;
		$this->ecotaxe = 0.00;
		$this->totalExpeditionTtc = 0.00;
		$this->totalExpeditionHt = 0.00;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set factureId
	 *
	 * @param integer $factureId
	 *
	 * @return LigneCommande
	 */
	public function setFactureId($factureId)
	{
		$this->factureId = $factureId;

		return $this;
	}

	/**
	 * Get factureId
	 *
	 * @return integer
	 */
	public function getFactureId()
	{
		return $this->factureId;
	}

	/**
	 * Set produitNom
	 *
	 * @param string $produitNom
	 *
	 * @return LigneCommande
	 */
	public function setProduitNom($produitNom)
	{
		$this->produitNom = $produitNom;

		return $this;
	}

	/**
	 * Get produitNom
	 *
	 * @return string
	 */
	public function getProduitNom()
	{
		return $this->produitNom;
	}

	/**
	 * Set produitReference
	 *
	 * @param string $produitReference
	 *
	 * @return LigneCommande
	 */
	public function setProduitReference($produitReference)
	{
		$this->produitReference = $produitReference;

		return $this;
	}

	/**
	 * Get produitReference
	 *
	 * @return string
	 */
	public function getProduitReference()
	{
		return $this->produitReference;
	}

	/**
	 * Set quantite
	 *
	 * @param integer $quantite
	 *
	 * @return LigneCommande
	 */
	public function setQuantite($quantite)
	{
		$this->quantite = $quantite;

		return $this;
	}

	/**
	 * Get quantite
	 *
	 * @return integer
	 */
	public function getQuantite()
	{
		return $this->quantite;
	}

	/**
	 * Set prixUnitaireTtc
	 *
	 * @param string $prixUnitaireTtc
	 *
	 * @return LigneCommande
	 */
	public function setPrixUnitaireTtc($prixUnitaireTtc)
	{
		$this->prixUnitaireTtc = $prixUnitaireTtc;

		return $this;
	}

	/**
	 * Get prixUnitaireTtc
	 *
	 * @return string
	 */
	public function getPrixUnitaireTtc()
	{
		return $this->prixUnitaireTtc;
	}

	/**
	 * Set prixUnitaireHt
	 *
	 * @param string $prixUnitaireHt
	 *
	 * @return LigneCommande
	 */
	public function setPrixUnitaireHt($prixUnitaireHt)
	{
		$this->prixUnitaireHt = $prixUnitaireHt;

		return $this;
	}

	/**
	 * Get prixUnitaireHt
	 *
	 * @return string
	 */
	public function getPrixUnitaireHt()
	{
		return $this->prixUnitaireHt;
	}

	/**
	 * Set totalPrixTtc
	 *
	 * @param string $totalPrixTtc
	 *
	 * @return LigneCommande
	 */
	public function setTotalPrixTtc($totalPrixTtc)
	{
		$this->totalPrixTtc = $totalPrixTtc;

		return $this;
	}

	/**
	 * Get totalPrixTtc
	 *
	 * @return string
	 */
	public function getTotalPrixTtc()
	{
		return $this->totalPrixTtc;
	}

	/**
	 * Set totalPrixHt
	 *
	 * @param string $totalPrixHt
	 *
	 * @return LigneCommande
	 */
	public function setTotalPrixHt($totalPrixHt)
	{
		$this->totalPrixHt = $totalPrixHt;

		return $this;
	}

	/**
	 * Get totalPrixHt
	 *
	 * @return string
	 */
	public function getTotalPrixHt()
	{
		return $this->totalPrixHt;
	}

	/**
	 * Set reductionPourcent
	 *
	 * @param string $reductionPourcent
	 *
	 * @return LigneCommande
	 */
	public function setReductionPourcent($reductionPourcent)
	{
		$this->reductionPourcent = $reductionPourcent;

		return $this;
	}

	/**
	 * Get reductionPourcent
	 *
	 * @return string
	 */
	public function getReductionPourcent()
	{
		return $this->reductionPourcent;
	}

	/**
	 * Set reductionMontantTtc
	 *
	 * @param string $reductionMontantTtc
	 *
	 * @return LigneCommande
	 */
	public function setReductionMontantTtc($reductionMontantTtc)
	{
		$this->reductionMontantTtc = $reductionMontantTtc;

		return $this;
	}

	/**
	 * Get reductionMontantTtc
	 *
	 * @return string
	 */
	public function getReductionMontantTtc()
	{
		return $this->reductionMontantTtc;
	}

	/**
	 * Set reductionMontantHt
	 *
	 * @param string $reductionMontantHt
	 *
	 * @return LigneCommande
	 */
	public function setReductionMontantHt($reductionMontantHt)
	{
		$this->reductionMontantHt = $reductionMontantHt;

		return $this;
	}

	/**
	 * Get reductionMontantHt
	 *
	 * @return string
	 */
	public function getReductionMontantHt()
	{
		return $this->reductionMontantHt;
	}

	/**
	 * Set taxeNom
	 *
	 * @param string $taxeNom
	 *
	 * @return LigneCommande
	 */
	public function setTaxeNom($taxeNom)
	{
		$this->taxeNom = $taxeNom;

		return $this;
	}

	/**
	 * Get taxeNom
	 *
	 * @return string
	 */
	public function getTaxeNom()
	{
		return $this->taxeNom;
	}

	/**
	 * Set taxeTaux
	 *
	 * @param string $taxeTaux
	 *
	 * @return LigneCommande
	 */
	public function setTaxeTaux($taxeTaux)
	{
		$this->taxeTaux = $taxeTaux;

		return $this;
	}

	/**
	 * Get taxeTaux
	 *
	 * @return string
	 */
	public function getTaxeTaux()
	{
		return $this->taxeTaux;
	}

	/**
	 * Set ecotaxe
	 *
	 * @param string $ecotaxe
	 *
	 * @return LigneCommande
	 */
	public function setEcotaxe($ecotaxe)
	{
		$this->ecotaxe = $ecotaxe;

		return $this;
	}

	/**
	 * Get ecotaxe
	 *
	 * @return string
	 */
	public function getEcotaxe()
	{
		return $this->ecotaxe;
	}

	/**
	 * Set totalExpeditionTtc
	 *
	 * @param string $totalExpeditionTtc
	 *
	 * @return LigneCommande
	 */
	public function setTotalExpeditionTtc($totalExpeditionTtc)
	{
		$this->totalExpeditionTtc = $totalExpeditionTtc;

		return $this;
	}

	/**
	 * Get totalExpeditionTtc
	 *
	 * @return string
	 */
	public function getTotalExpeditionTtc()
	{
		return $this->totalExpeditionTtc;
	}

	/**
	 * Set totalExpeditionHt
	 *
	 * @param string $totalExpeditionHt
	 *
	 * @return LigneCommande
	 */
	public function setTotalExpeditionHt($totalExpeditionHt)
	{
		$this->totalExpeditionHt = $totalExpeditionHt;

		return $this;
	}

	/**
	 * Get totalExpeditionHt
	 *
	 * @return string
	 */
	public function getTotalExpeditionHt()
	{
		return $this->totalExpeditionHt;
	}

	/**
	 * Set commande
	 *
	 * @param \AppBundle\Entity\Boutique\Commande $commande
	 *
	 * @return LigneCommande
	 */
	public function setCommande(\AppBundle\Entity\Boutique\Commande $commande)
	{
		$this->commande = $commande;

		return $this;
	}

	/**
	 * Get commande
	 *
	 * @return \AppBundle\Entity\Boutique\Commande
	 */
	public function getCommande()
	{
		return $this->commande;
	}



    /**
     * Set wine.
     *
     * @param \AppBundle\Entity\Wine $wine
     *
     * @return LigneCommande
     */
    public function setWine(\AppBundle\Entity\Wine $wine)
    {
        $this->wine = $wine;

        return $this;
    }

    /**
     * Get wine.
     *
     * @return \AppBundle\Entity\Wine
     */
    public function getWine()
    {
        return $this->wine;
    }
}
