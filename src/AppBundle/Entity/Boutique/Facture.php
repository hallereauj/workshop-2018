<?php

namespace AppBundle\Entity\Boutique;

use Doctrine\ORM\Mapping as ORM;


/**
 * Facture
 *
 * @ORM\Table(name="btq_facture")
 * @ORM\Entity
 */
class Facture
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Boutique\Commande")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $commande;



	/**
	 * @var integer
	 *
	 * @ORM\Column(name="numero", type="integer", nullable=true)
	 */
	private $numero;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="numero_livraison", type="integer", nullable=true)
	 */
	private $numeroLivraison;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="livraison_at", type="datetime", nullable=true)
	 */
	private $livraisonAt;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_reductions_ttc", type="decimal", precision=17, scale=2)
	 */
	private $totalReductionsTtc;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_reductions_ht", type="decimal", precision=17, scale=2)
	 */
	private $totalReductionsHt;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_paye_ttc", type="decimal", precision=17, scale=2)
	 */
	private $totalPayeTtc;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_paye_ht", type="decimal", precision=17, scale=2)
	 */
	private $totalPayeHt;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_produits_ttc", type="decimal", precision=17, scale=2)
	 */
	private $totalProduitsTtc;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_produits_ht", type="decimal", precision=17, scale=2)
	 */
	private $totalProduitsHt;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_expedition_ttc", type="decimal", precision=17, scale=2)
	 */
	private $totalExpeditionTtc;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_expedition_ht", type="decimal", precision=17, scale=2)
	 */
	private $totalExpeditionHt;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="commentaires", type="text", nullable=true)
	 */
	private $commentaires;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created_at", type="datetime")
	 */
	private $createdAt;



	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->totalReductionsTtc = 0.00;
		$this->totalReductionsHt = 0.00;
		$this->totalPayeTtc = 0.00;
		$this->totalPayeHt = 0.00;
		$this->totalProduitsTtc = 0.00;
		$this->totalProduitsHt = 0.00;
		$this->totalExpeditionTtc = 0.00;
		$this->totalExpeditionHt = 0.00;
		$this->createdAt = new \DateTime();
	}



	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set numero
	 *
	 * @param integer $numero
	 *
	 * @return Facture
	 */
	public function setNumero($numero)
	{
		$this->numero = $numero;

		return $this;
	}

	/**
	 * Get numero
	 *
	 * @return integer
	 */
	public function getNumero()
	{
		return $this->numero;
	}

	/**
	 * Set numeroLivraison
	 *
	 * @param integer $numeroLivraison
	 *
	 * @return Facture
	 */
	public function setNumeroLivraison($numeroLivraison)
	{
		$this->numeroLivraison = $numeroLivraison;

		return $this;
	}

	/**
	 * Get numeroLivraison
	 *
	 * @return integer
	 */
	public function getNumeroLivraison()
	{
		return $this->numeroLivraison;
	}

	/**
	 * Set livraisonAt
	 *
	 * @param \DateTime $livraisonAt
	 *
	 * @return Facture
	 */
	public function setLivraisonAt($livraisonAt)
	{
		$this->livraisonAt = $livraisonAt;

		return $this;
	}

	/**
	 * Get livraisonAt
	 *
	 * @return \DateTime
	 */
	public function getLivraisonAt()
	{
		return $this->livraisonAt;
	}

	/**
	 * Set totalReductionsTtc
	 *
	 * @param string $totalReductionsTtc
	 *
	 * @return Facture
	 */
	public function setTotalReductionsTtc($totalReductionsTtc)
	{
		$this->totalReductionsTtc = $totalReductionsTtc;

		return $this;
	}

	/**
	 * Get totalReductionsTtc
	 *
	 * @return string
	 */
	public function getTotalReductionsTtc()
	{
		return $this->totalReductionsTtc;
	}

	/**
	 * Set totalReductionsHt
	 *
	 * @param string $totalReductionsHt
	 *
	 * @return Facture
	 */
	public function setTotalReductionsHt($totalReductionsHt)
	{
		$this->totalReductionsHt = $totalReductionsHt;

		return $this;
	}

	/**
	 * Get totalReductionsHt
	 *
	 * @return string
	 */
	public function getTotalReductionsHt()
	{
		return $this->totalReductionsHt;
	}

	/**
	 * Set totalPayeTtc
	 *
	 * @param string $totalPayeTtc
	 *
	 * @return Facture
	 */
	public function setTotalPayeTtc($totalPayeTtc)
	{
		$this->totalPayeTtc = $totalPayeTtc;

		return $this;
	}

	/**
	 * Get totalPayeTtc
	 *
	 * @return string
	 */
	public function getTotalPayeTtc()
	{
		return $this->totalPayeTtc;
	}

	/**
	 * Set totalPayeHt
	 *
	 * @param string $totalPayeHt
	 *
	 * @return Facture
	 */
	public function setTotalPayeHt($totalPayeHt)
	{
		$this->totalPayeHt = $totalPayeHt;

		return $this;
	}

	/**
	 * Get totalPayeHt
	 *
	 * @return string
	 */
	public function getTotalPayeHt()
	{
		return $this->totalPayeHt;
	}

	/**
	 * Set totalProduitsTtc
	 *
	 * @param string $totalProduitsTtc
	 *
	 * @return Facture
	 */
	public function setTotalProduitsTtc($totalProduitsTtc)
	{
		$this->totalProduitsTtc = $totalProduitsTtc;

		return $this;
	}

	/**
	 * Get totalProduitsTtc
	 *
	 * @return string
	 */
	public function getTotalProduitsTtc()
	{
		return $this->totalProduitsTtc;
	}

	/**
	 * Set totalProduitsHt
	 *
	 * @param string $totalProduitsHt
	 *
	 * @return Facture
	 */
	public function setTotalProduitsHt($totalProduitsHt)
	{
		$this->totalProduitsHt = $totalProduitsHt;

		return $this;
	}

	/**
	 * Get totalProduitsHt
	 *
	 * @return string
	 */
	public function getTotalProduitsHt()
	{
		return $this->totalProduitsHt;
	}

	/**
	 * Set totalExpeditionTtc
	 *
	 * @param string $totalExpeditionTtc
	 *
	 * @return Facture
	 */
	public function setTotalExpeditionTtc($totalExpeditionTtc)
	{
		$this->totalExpeditionTtc = $totalExpeditionTtc;

		return $this;
	}

	/**
	 * Get totalExpeditionTtc
	 *
	 * @return string
	 */
	public function getTotalExpeditionTtc()
	{
		return $this->totalExpeditionTtc;
	}

	/**
	 * Set totalExpeditionHt
	 *
	 * @param string $totalExpeditionHt
	 *
	 * @return Facture
	 */
	public function setTotalExpeditionHt($totalExpeditionHt)
	{
		$this->totalExpeditionHt = $totalExpeditionHt;

		return $this;
	}

	/**
	 * Get totalExpeditionHt
	 *
	 * @return string
	 */
	public function getTotalExpeditionHt()
	{
		return $this->totalExpeditionHt;
	}

	/**
	 * Set commentaires
	 *
	 * @param string $commentaires
	 *
	 * @return Facture
	 */
	public function setCommentaires($commentaires)
	{
		$this->commentaires = $commentaires;

		return $this;
	}

	/**
	 * Get commentaires
	 *
	 * @return string
	 */
	public function getCommentaires()
	{
		return $this->commentaires;
	}

	/**
	 * Set createdAt
	 *
	 * @param \DateTime $createdAt
	 *
	 * @return Facture
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * Get createdAt
	 *
	 * @return \DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * Set commande
	 *
	 * @param \AppBundle\Entity\Boutique\Commande $commande
	 *
	 * @return Facture
	 */
	public function setCommande(\AppBundle\Entity\Boutique\Commande $commande)
	{
		$this->commande = $commande;

		return $this;
	}

	/**
	 * Get commande
	 *
	 * @return \AppBundle\Entity\Boutique\Commande
	 */
	public function getCommande()
	{
		return $this->commande;
	}
}
