<?php

namespace AppBundle\Entity\Boutique;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Commande
 *
 * @ORM\Table(name="btq_commande")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Boutique\CommandeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Commande
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $user;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Adresse")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $adresseLivraison;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Adresse")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $adresseFacturation;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Boutique\Statut")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $statut;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Boutique\ReponseAcquereur")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $reponseAcquereur;

	/**
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Boutique\LigneCommande", mappedBy="commande")
	 */
	private $ligneCommandes;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="reference", type="string", length=12, nullable=true)
	 */
	private $reference;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cle", type="string", length=32)
	 */
	private $cle;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="paiement", type="string", length=255, nullable=true)
	 */
	private $paiement;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="commentaires", type="text", nullable=true)
	 */
	private $commentaires;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="numero_expedition", type="string", length=32, nullable=true)
	 */
	private $numeroExpedition;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_reductions_ttc", type="decimal", precision=17, scale=2)
	 */
	private $totalReductionsTtc;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_reductions_ht", type="decimal", precision=17, scale=2)
	 */
	private $totalReductionsHt;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_paye_ttc", type="decimal", precision=17, scale=2)
	 */
	private $totalPayeTtc;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_paye_ht", type="decimal", precision=17, scale=2)
	 */
	private $totalPayeHt;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_paye_reel", type="decimal", precision=17, scale=2)
	 */
	private $totalPayeReel;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_produits_ttc", type="decimal", precision=17, scale=2)
	 */
	private $totalProduitsTtc;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_produits_ht", type="decimal", precision=17, scale=2)
	 */
	private $totalProduitsHt;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_expedition_ttc", type="decimal", precision=17, scale=2)
	 */
	private $totalExpeditionTtc;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_expedition_ht", type="decimal", precision=17, scale=2)
	 */
	private $totalExpeditionHt;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="numero_facture", type="integer", nullable=true)
	 */
	private $numeroFacture;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="numero_livraison", type="integer", nullable=true)
	 */
	private $numeroLivraison;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="facture_created_at", type="datetime", nullable=true)
	 */
	private $FactureCreatedAt;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="livraison_at", type="datetime", nullable=true)
	 */
	private $LivraisonAt;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_valide", type="boolean")
	 */
	private $isValide;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created_at", type="datetime")
	 */
	private $createdAt;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="updated_at", type="datetime", nullable=true)
	 */
	private $updatedAt;



	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->totalReductionsTtc = 0.00;
		$this->totalReductionsHt = 0.00;
		$this->totalPayeTtc = 0.00;
		$this->totalPayeHt = 0.00;
		$this->totalPayeReel = 0.00;
		$this->totalProduitsTtc = 0.00;
		$this->totalProduitsHt = 0.00;
		$this->totalExpeditionTtc = 0.00;
		$this->totalExpeditionHt = 0.00;
		$this->isValide = false;
		$this->createdAt = new \DateTime();

		$this->lignesCommande = new ArrayCollection();
	}

	/**
	 * @ORM\PreUpdate
	 */
	public function updateDate()
	{
		$this->setUpdatedAt(new \DateTime());
	}




	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set reference
	 *
	 * @param string $reference
	 *
	 * @return Commande
	 */
	public function setReference($reference)
	{
		$this->reference = $reference;

		return $this;
	}

	/**
	 * Get reference
	 *
	 * @return string
	 */
	public function getReference()
	{
		return $this->reference;
	}

	/**
	 * Set cle
	 *
	 * @param string $cle
	 *
	 * @return Commande
	 */
	public function setCle($cle)
	{
		$this->cle = $cle;

		return $this;
	}

	/**
	 * Get cle
	 *
	 * @return string
	 */
	public function getCle()
	{
		return $this->cle;
	}

	/**
	 * Set paiement
	 *
	 * @param string $paiement
	 *
	 * @return Commande
	 */
	public function setPaiement($paiement)
	{
		$this->paiement = $paiement;

		return $this;
	}

	/**
	 * Get paiement
	 *
	 * @return string
	 */
	public function getPaiement()
	{
		return $this->paiement;
	}

	/**
	 * Set commentaires
	 *
	 * @param string $commentaires
	 *
	 * @return Commande
	 */
	public function setCommentaires($commentaires)
	{
		$this->commentaires = $commentaires;

		return $this;
	}

	/**
	 * Get commentaires
	 *
	 * @return string
	 */
	public function getCommentaires()
	{
		return $this->commentaires;
	}

	/**
	 * Set numeroExpedition
	 *
	 * @param string $numeroExpedition
	 *
	 * @return Commande
	 */
	public function setNumeroExpedition($numeroExpedition)
	{
		$this->numeroExpedition = $numeroExpedition;

		return $this;
	}

	/**
	 * Get numeroExpedition
	 *
	 * @return string
	 */
	public function getNumeroExpedition()
	{
		return $this->numeroExpedition;
	}

	/**
	 * Set totalReductionsTtc
	 *
	 * @param string $totalReductionsTtc
	 *
	 * @return Commande
	 */
	public function setTotalReductionsTtc($totalReductionsTtc)
	{
		$this->totalReductionsTtc = $totalReductionsTtc;

		return $this;
	}

	/**
	 * Get totalReductionsTtc
	 *
	 * @return string
	 */
	public function getTotalReductionsTtc()
	{
		return $this->totalReductionsTtc;
	}

	/**
	 * Set totalReductionsHt
	 *
	 * @param string $totalReductionsHt
	 *
	 * @return Commande
	 */
	public function setTotalReductionsHt($totalReductionsHt)
	{
		$this->totalReductionsHt = $totalReductionsHt;

		return $this;
	}

	/**
	 * Get totalReductionsHt
	 *
	 * @return string
	 */
	public function getTotalReductionsHt()
	{
		return $this->totalReductionsHt;
	}

	/**
	 * Set totalPayeTtc
	 *
	 * @param string $totalPayeTtc
	 *
	 * @return Commande
	 */
	public function setTotalPayeTtc($totalPayeTtc)
	{
		$this->totalPayeTtc = $totalPayeTtc;

		return $this;
	}

	/**
	 * Get totalPayeTtc
	 *
	 * @return string
	 */
	public function getTotalPayeTtc()
	{
		return $this->totalPayeTtc;
	}

	/**
	 * Set totalPayeHt
	 *
	 * @param string $totalPayeHt
	 *
	 * @return Commande
	 */
	public function setTotalPayeHt($totalPayeHt)
	{
		$this->totalPayeHt = $totalPayeHt;

		return $this;
	}

	/**
	 * Get totalPayeHt
	 *
	 * @return string
	 */
	public function getTotalPayeHt()
	{
		return $this->totalPayeHt;
	}

	/**
	 * Set totalPayeReel
	 *
	 * @param string $totalPayeReel
	 *
	 * @return Commande
	 */
	public function setTotalPayeReel($totalPayeReel)
	{
		$this->totalPayeReel = $totalPayeReel;

		return $this;
	}

	/**
	 * Get totalPayeReel
	 *
	 * @return string
	 */
	public function getTotalPayeReel()
	{
		return $this->totalPayeReel;
	}

	/**
	 * Set totalProduitsTtc
	 *
	 * @param string $totalProduitsTtc
	 *
	 * @return Commande
	 */
	public function setTotalProduitsTtc($totalProduitsTtc)
	{
		$this->totalProduitsTtc = $totalProduitsTtc;

		return $this;
	}

	/**
	 * Get totalProduitsTtc
	 *
	 * @return string
	 */
	public function getTotalProduitsTtc()
	{
		return $this->totalProduitsTtc;
	}

	/**
	 * Set totalProduitsHt
	 *
	 * @param string $totalProduitsHt
	 *
	 * @return Commande
	 */
	public function setTotalProduitsHt($totalProduitsHt)
	{
		$this->totalProduitsHt = $totalProduitsHt;

		return $this;
	}

	/**
	 * Get totalProduitsHt
	 *
	 * @return string
	 */
	public function getTotalProduitsHt()
	{
		return $this->totalProduitsHt;
	}

	/**
	 * Set totalExpeditionTtc
	 *
	 * @param string $totalExpeditionTtc
	 *
	 * @return Commande
	 */
	public function setTotalExpeditionTtc($totalExpeditionTtc)
	{
		$this->totalExpeditionTtc = $totalExpeditionTtc;

		return $this;
	}

	/**
	 * Get totalExpeditionTtc
	 *
	 * @return string
	 */
	public function getTotalExpeditionTtc()
	{
		return $this->totalExpeditionTtc;
	}

	/**
	 * Set totalExpeditionHt
	 *
	 * @param string $totalExpeditionHt
	 *
	 * @return Commande
	 */
	public function setTotalExpeditionHt($totalExpeditionHt)
	{
		$this->totalExpeditionHt = $totalExpeditionHt;

		return $this;
	}

	/**
	 * Get totalExpeditionHt
	 *
	 * @return string
	 */
	public function getTotalExpeditionHt()
	{
		return $this->totalExpeditionHt;
	}

	/**
	 * Set numeroFacture
	 *
	 * @param integer $numeroFacture
	 *
	 * @return Commande
	 */
	public function setNumeroFacture($numeroFacture)
	{
		$this->numeroFacture = $numeroFacture;

		return $this;
	}

	/**
	 * Get numeroFacture
	 *
	 * @return integer
	 */
	public function getNumeroFacture()
	{
		return $this->numeroFacture;
	}

	/**
	 * Set numeroLivraison
	 *
	 * @param integer $numeroLivraison
	 *
	 * @return Commande
	 */
	public function setNumeroLivraison($numeroLivraison)
	{
		$this->numeroLivraison = $numeroLivraison;

		return $this;
	}

	/**
	 * Get numeroLivraison
	 *
	 * @return integer
	 */
	public function getNumeroLivraison()
	{
		return $this->numeroLivraison;
	}

	/**
	 * Set factureCreatedAt
	 *
	 * @param \DateTime $factureCreatedAt
	 *
	 * @return Commande
	 */
	public function setFactureCreatedAt($factureCreatedAt)
	{
		$this->factureCreatedAt = $factureCreatedAt;

		return $this;
	}

	/**
	 * Get factureCreatedAt
	 *
	 * @return \DateTime
	 */
	public function getFactureCreatedAt()
	{
		return $this->factureCreatedAt;
	}

	/**
	 * Set livraisonAt
	 *
	 * @param \DateTime $livraisonAt
	 *
	 * @return Commande
	 */
	public function setLivraisonAt($livraisonAt)
	{
		$this->livraisonAt = $livraisonAt;

		return $this;
	}

	/**
	 * Get livraisonAt
	 *
	 * @return \DateTime
	 */
	public function getLivraisonAt()
	{
		return $this->livraisonAt;
	}

	/**
	 * Set isValide
	 *
	 * @param boolean $isValide
	 *
	 * @return Commande
	 */
	public function setIsValide($isValide)
	{
		$this->isValide = $isValide;

		return $this;
	}

	/**
	 * Get isValide
	 *
	 * @return boolean
	 */
	public function getIsValide()
	{
		return $this->isValide;
	}

	/**
	 * Set createdAt
	 *
	 * @param \DateTime $createdAt
	 *
	 * @return Commande
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * Get createdAt
	 *
	 * @return \DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * Set updatedAt
	 *
	 * @param \DateTime $updatedAt
	 *
	 * @return Commande
	 */
	public function setUpdatedAt($updatedAt)
	{
		$this->updatedAt = $updatedAt;

		return $this;
	}

	/**
	 * Get updatedAt
	 *
	 * @return \DateTime
	 */
	public function getUpdatedAt()
	{
		return $this->updatedAt;
	}

	/**
	 * Set user
	 *
	 * @param \AppBundle\Entity\User $user
	 *
	 * @return Commande
	 */
	public function setUser(\AppBundle\Entity\User $user)
	{
		$this->user = $user;

		return $this;
	}

	/**
	 * Get user
	 *
	 * @return \AppBundle\Entity\User
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * Set adresseLivraison
	 *
	 * @param \AppBundle\Entity\Adresse $adresseLivraison
	 *
	 * @return Commande
	 */
	public function setAdresseLivraison(\AppBundle\Entity\Adresse $adresseLivraison)
	{
		$this->adresseLivraison = $adresseLivraison;

		return $this;
	}

	/**
	 * Get adresseLivraison
	 *
	 * @return \AppBundle\Entity\Adresse
	 */
	public function getAdresseLivraison()
	{
		return $this->adresseLivraison;
	}

	/**
	 * Set adresseFacturation
	 *
	 * @param \AppBundle\Entity\Adresse $adresseFacturation
	 *
	 * @return Commande
	 */
	public function setAdresseFacturation(\AppBundle\Entity\Adresse $adresseFacturation)
	{
		$this->adresseFacturation = $adresseFacturation;

		return $this;
	}

	/**
	 * Get adresseFacturation
	 *
	 * @return \AppBundle\Entity\Adresse
	 */
	public function getAdresseFacturation()
	{
		return $this->adresseFacturation;
	}

	/**
	 * Set statut
	 *
	 * @param \AppBundle\Entity\Boutique\Statut $statut
	 *
	 * @return Commande
	 */
	public function setStatut(\AppBundle\Entity\Boutique\Statut $statut)
	{
		$this->statut = $statut;

		return $this;
	}

	/**
	 * Get statut
	 *
	 * @return \AppBundle\Entity\Boutique\Statut
	 */
	public function getStatut()
	{
		return $this->statut;
	}

	/**
	 * Set reponseAcquereur
	 *
	 * @param \AppBundle\Entity\Boutique\ReponseAcquereur $reponseAcquereur
	 *
	 * @return Commande
	 */
	public function setReponseAcquereur(\AppBundle\Entity\Boutique\ReponseAcquereur $reponseAcquereur = null)
	{
		$this->reponseAcquereur = $reponseAcquereur;

		return $this;
	}

	/**
	 * Get reponseAcquereur
	 *
	 * @return \AppBundle\Entity\Boutique\ReponseAcquereur
	 */
	public function getReponseAcquereur()
	{
		return $this->reponseAcquereur;
	}


	/**
	 * Add ligneCommande
	 *
	 * @param \AppBundle\Entity\Boutique\LigneCommande $ligneCommande
	 *
	 * @return Commande
	 */
	public function addLigneCommande(\AppBundle\Entity\Boutique\LigneCommande $ligneCommande)
	{
		$this->ligneCommandes[] = $ligneCommande;
		$ligneCommande->setCommande($this);

		return $this;
	}

	/**
	 * Remove ligneCommande
	 *
	 * @param \AppBundle\Entity\Boutique\LigneCommande $ligneCommande
	 */
	public function removeLigneCommande(\AppBundle\Entity\Boutique\LigneCommande $ligneCommande)
	{
		$this->ligneCommandes->removeElement($ligneCommande);
	}

	/**
	 * Get ligneCommandes
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getLigneCommandes()
	{
		return $this->ligneCommandes;
	}
}
