<?php

namespace AppBundle\Entity\Boutique;

use Doctrine\ORM\Mapping as ORM;


/**
 * ReponseAcquereur
 *
 * @ORM\Table(name="btq_reponseacquereur")
 * @ORM\Entity
 */
class ReponseAcquereur
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="code", type="string", length=2)
	 */
	private $code;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="signification", type="text")
	 */
	private $signification;



	/**
	 * Constructor
	 */
	public function __construct()
	{
		// RAS
	}



	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set code
	 *
	 * @param string $code
	 *
	 * @return ReponseAcquereur
	 */
	public function setCode($code)
	{
		$this->code = $code;

		return $this;
	}

	/**
	 * Get code
	 *
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}

	/**
	 * Set signification
	 *
	 * @param string $signification
	 *
	 * @return ReponseAcquereur
	 */
	public function setSignification($signification)
	{
		$this->signification = $signification;

		return $this;
	}

	/**
	 * Get signification
	 *
	 * @return string
	 */
	public function getSignification()
	{
		return $this->signification;
	}
}
