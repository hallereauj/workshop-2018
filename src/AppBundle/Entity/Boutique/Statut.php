<?php

namespace AppBundle\Entity\Boutique;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Statut
 *
 * @ORM\Table(name="btq_statut")
 * @ORM\Entity
 */
class Statut
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nom", type="string", length=255)
	 */
	private $nom;

	/**
	 * @Gedmo\Slug(fields={"nom"})
	 * @ORM\Column(length=128, unique=true)
	 */
	private $slug;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="template", type="string", length=255, nullable=true)
	 */
	private $template;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="invoice", type="boolean")
	 */
	private $invoice;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="send_email", type="boolean")
	 */
	private $sendEmail;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="color", type="string", length=32)
	 */
	private $color;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="unremovable", type="boolean")
	 */
	private $unremovable;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="hidden", type="boolean")
	 */
	private $hidden;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="logable", type="boolean")
	 */
	private $logable;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="delivery", type="boolean")
	 */
	private $delivery;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="shipped", type="boolean")
	 */
	private $shipped;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="paid", type="boolean")
	 */
	private $paid;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
	 */
	private $deletedAt;


	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->deletedAt = null;

	}


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set nom
	 *
	 * @param string $nom
	 *
	 * @return Statut
	 */
	public function setNom($nom)
	{
		$this->nom = $nom;

		return $this;
	}

	/**
	 * Get nom
	 *
	 * @return string
	 */
	public function getNom()
	{
		return $this->nom;
	}

	/**
	 * Set slug
	 *
	 * @param string $slug
	 *
	 * @return Statut
	 */
	public function setSlug($slug)
	{
		$this->slug = $slug;

		return $this;
	}

	/**
	 * Get slug
	 *
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}

	/**
	 * Set template
	 *
	 * @param string $template
	 *
	 * @return Statut
	 */
	public function setTemplate($template)
	{
		$this->template = $template;

		return $this;
	}

	/**
	 * Get template
	 *
	 * @return string
	 */
	public function getTemplate()
	{
		return $this->template;
	}

	/**
	 * Set invoice
	 *
	 * @param boolean $invoice
	 *
	 * @return Statut
	 */
	public function setInvoice($invoice)
	{
		$this->invoice = $invoice;

		return $this;
	}

	/**
	 * Get invoice
	 *
	 * @return boolean
	 */
	public function getInvoice()
	{
		return $this->invoice;
	}

	/**
	 * Set sendEmail
	 *
	 * @param boolean $sendEmail
	 *
	 * @return Statut
	 */
	public function setSendEmail($sendEmail)
	{
		$this->sendEmail = $sendEmail;

		return $this;
	}

	/**
	 * Get sendEmail
	 *
	 * @return boolean
	 */
	public function getSendEmail()
	{
		return $this->sendEmail;
	}

	/**
	 * Set color
	 *
	 * @param string $color
	 *
	 * @return Statut
	 */
	public function setColor($color)
	{
		$this->color = $color;

		return $this;
	}

	/**
	 * Get color
	 *
	 * @return string
	 */
	public function getColor()
	{
		return $this->color;
	}

	/**
	 * Set unremovable
	 *
	 * @param boolean $unremovable
	 *
	 * @return Statut
	 */
	public function setUnremovable($unremovable)
	{
		$this->unremovable = $unremovable;

		return $this;
	}

	/**
	 * Get unremovable
	 *
	 * @return boolean
	 */
	public function getUnremovable()
	{
		return $this->unremovable;
	}

	/**
	 * Set hidden
	 *
	 * @param boolean $hidden
	 *
	 * @return Statut
	 */
	public function setHidden($hidden)
	{
		$this->hidden = $hidden;

		return $this;
	}

	/**
	 * Get hidden
	 *
	 * @return boolean
	 */
	public function getHidden()
	{
		return $this->hidden;
	}

	/**
	 * Set logable
	 *
	 * @param boolean $logable
	 *
	 * @return Statut
	 */
	public function setLogable($logable)
	{
		$this->logable = $logable;

		return $this;
	}

	/**
	 * Get logable
	 *
	 * @return boolean
	 */
	public function getLogable()
	{
		return $this->logable;
	}

	/**
	 * Set delivery
	 *
	 * @param boolean $delivery
	 *
	 * @return Statut
	 */
	public function setDelivery($delivery)
	{
		$this->delivery = $delivery;

		return $this;
	}

	/**
	 * Get delivery
	 *
	 * @return boolean
	 */
	public function getDelivery()
	{
		return $this->delivery;
	}

	/**
	 * Set shipped
	 *
	 * @param boolean $shipped
	 *
	 * @return Statut
	 */
	public function setShipped($shipped)
	{
		$this->shipped = $shipped;

		return $this;
	}

	/**
	 * Get shipped
	 *
	 * @return boolean
	 */
	public function getShipped()
	{
		return $this->shipped;
	}

	/**
	 * Set paid
	 *
	 * @param boolean $paid
	 *
	 * @return Statut
	 */
	public function setPaid($paid)
	{
		$this->paid = $paid;

		return $this;
	}

	/**
	 * Get paid
	 *
	 * @return boolean
	 */
	public function getPaid()
	{
		return $this->paid;
	}

	/**
	 * Set deletedAt
	 *
	 * @param \DateTime $deletedAt
	 *
	 * @return Statut
	 */
	public function setDeletedAt($deletedAt)
	{
		$this->deletedAt = $deletedAt;

		return $this;
	}

	/**
	 * Get deletedAt
	 *
	 * @return \DateTime
	 */
	public function getDeletedAt()
	{
		return $this->deletedAt;
	}
}
