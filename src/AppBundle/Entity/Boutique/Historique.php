<?php

namespace AppBundle\Entity\Boutique;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historique
 *
 * @ORM\Table(name="btq_historique")
 * @ORM\Entity
 */
class Historique
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $user;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Boutique\Commande")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $commande;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Boutique\Statut")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $statut;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="statut_nom", type="string", length=128)
	 */
	private $statutNom;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created_at", type="datetime")
	 */
	private $createdAt;



	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->createdAt = new \DateTime();
	}



	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set createdAt
	 *
	 * @param \DateTime $createdAt
	 *
	 * @return Historique
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * Get createdAt
	 *
	 * @return \DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * Set user
	 *
	 * @param \AppBundle\Entity\User $user
	 *
	 * @return Historique
	 */
	public function setUser(\AppBundle\Entity\User $user = null)
	{
		$this->user = $user;

		return $this;
	}

	/**
	 * Get user
	 *
	 * @return \AppBundle\Entity\User
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * Set commande
	 *
	 * @param \AppBundle\Entity\Boutique\Commande $commande
	 *
	 * @return Historique
	 */
	public function setCommande(\AppBundle\Entity\Boutique\Commande $commande)
	{
		$this->commande = $commande;

		return $this;
	}

	/**
	 * Get commande
	 *
	 * @return \AppBundle\Entity\Boutique\Commande
	 */
	public function getCommande()
	{
		return $this->commande;
	}

	/**
	 * Set statut
	 *
	 * @param \AppBundle\Entity\Boutique\Statut $statut
	 *
	 * @return Historique
	 */
	public function setStatut(\AppBundle\Entity\Boutique\Statut $statut)
	{
		$this->statut = $statut;

		return $this;
	}

	/**
	 * Get statut
	 *
	 * @return \AppBundle\Entity\Boutique\Statut
	 */
	public function getStatut()
	{
		return $this->statut;
	}

	/**
	 * Set statutNom
	 *
	 * @param string $statutNom
	 *
	 * @return Historique
	 */
	public function setStatutNom($statutNom)
	{
		$this->statutNom = $statutNom;

		return $this;
	}

	/**
	 * Get statutNom
	 *
	 * @return string
	 */
	public function getStatutNom()
	{
		return $this->statutNom;
	}
}
