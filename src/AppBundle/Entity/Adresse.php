<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Adresse
 *
 * @ORM\Table(name="adresse")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdresseRepository")
 */
class Adresse
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="adresse", type="text")
	 * @Assert\NotBlank(
	 *    message = "Votre adresse ne doit pas être vide."
	 * )
	 */
	private $adresse;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="code_postal", type="string", length=5)
	 * @Assert\NotBlank(
	 *    message = "Votre code postal ne doit pas être vide."
	 * )
	 * @Assert\Regex(
	 *    pattern = "/^[0-9]{5,5}$/",
	 *    match = true,
	 *    message = "Le code postal ne correspond pas au modèle suivant : 49300"
	 * )
	 */
	private $codePostal;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ville", type="string", length=64)
	 * @Assert\NotBlank(
	 *    message = "Votre ville ne doit pas être vide."
	 * )
	 * @Assert\Length(
	 *    min = "2",
	 *    max = "255",
	 *    minMessage = "Votre ville doit faire au moins {{ limit }} caractères.",
	 *    maxMessage = "Votre ville ne peut pas être plus long que {{ limit }} caractères."
	 * )
	 */
	private $ville;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="pays", type="string", length=64)
	 * @Assert\NotBlank(
	 *    message = "Votre pays ne doit pas être vide."
	 * )
	 */
	private $pays;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created_at", type="datetime")
	 */
	private $createdAt;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="updated_at", type="datetime", nullable=true)
	 */
	private $updatedAt;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
	 */
	private $deletedAt;



	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->pays = "FRANCE";
		$this->createdAt = new \DateTime();
	}

	public function __toString()
	{
		return sprintf( '%s - %s %s (%s)', $this->adresse, $this->codePostal, strtoupper($this->ville), strtoupper($this->pays) );
	}

	public function getDisplayName()
	{
		return sprintf( '%s - %s %s (%s)', $this->adresse, $this->codePostal, strtoupper($this->ville), strtoupper($this->pays) );
	}

	public function getUniqueName()
	{
		return sprintf( '%s - %s %s (%s)', $this->adresse, $this->codePostal, strtoupper($this->ville), strtoupper($this->pays) );
	}
	public function getUniqueNameBr()
	{
		return sprintf( '%s <br> %s %s (%s)', $this->adresse, $this->codePostal, strtoupper($this->ville), strtoupper($this->pays) );
	}


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}


	/**
	 * Set adresse1
	 *
	 * @param string $adresse
	 *
	 * @return Adresse
	 */
	public function setAdresse($adresse)
	{
		$this->adresse = $adresse;

		return $this;
	}

	/**
	 * Get adresse
	 *
	 * @return string
	 */
	public function getAdresse()
	{
		return $this->adresse;
	}


	/**
	 * Set codePostal
	 *
	 * @param string $codePostal
	 *
	 * @return Adresse
	 */
	public function setCodePostal($codePostal)
	{
		$this->codePostal = $codePostal;

		return $this;
	}

	/**
	 * Get codePostal
	 *
	 * @return string
	 */
	public function getCodePostal()
	{
		return $this->codePostal;
	}

	/**
	 * Set ville
	 *
	 * @param string $ville
	 *
	 * @return Adresse
	 */
	public function setVille($ville)
	{
		$this->ville = $ville;

		return $this;
	}

	/**
	 * Get ville
	 *
	 * @return string
	 */
	public function getVille()
	{
		return $this->ville;
	}

	/**
	 * Set pays
	 *
	 * @param string $pays
	 *
	 * @return Adresse
	 */
	public function setPays($pays)
	{
		$this->pays = $pays;

		return $this;
	}

	/**
	 * Get pays
	 *
	 * @return string
	 */
	public function getPays()
	{
		return $this->pays;
	}


	/**
	 * Set createdAt
	 *
	 * @param \DateTime $createdAt
	 *
	 * @return Adresse
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * Get createdAt
	 *
	 * @return \DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * Set updatedAt
	 *
	 * @param \DateTime $updatedAt
	 *
	 * @return Adresse
	 */
	public function setUpdatedAt($updatedAt)
	{
		$this->updatedAt = $updatedAt;

		return $this;
	}

	/**
	 * Get updatedAt
	 *
	 * @return \DateTime
	 */
	public function getUpdatedAt()
	{
		return $this->updatedAt;
	}

	/**
	 * Set deletedAt
	 *
	 * @param \DateTime $deletedAt
	 *
	 * @return Adresse
	 */
	public function setDeletedAt($deletedAt)
	{
		$this->deletedAt = $deletedAt;

		return $this;
	}

	/**
	 * Get deletedAt
	 *
	 * @return \DateTime
	 */
	public function getDeletedAt()
	{
		return $this->deletedAt;
	}
}
