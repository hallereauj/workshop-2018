<?php

namespace AppBundle\Service;

use AppBundle\Entity\Boutique\Paiement;
use AppBundle\Entity\Boutique\Statut;
use AppBundle\Entity\Wine;
use AppBundle\Utils\GLService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

use AppBundle\Entity\User;

use AppBundle\Entity\Boutique\Commande;
use AppBundle\Entity\Boutique\LigneCommande;
use AppBundle\Entity\Boutique\Historique;


class BoutiqueService
{
	private $em;
	private $glService;
	private $session;

	public function __construct(EntityManagerInterface $em, GLService $glService, SessionInterface $session)
	{
		$this->em = $em;
		$this->glService = $glService;
		$this->session = $session;
	}


// region PANIER //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function initialiserPanier ()
	{
		$this->session->set('panier', []);
	}

	public function getPanier()
	{
		// on recupere l'eventuel panier sinon on l'instancie à null
		return  $this->session->get('panier', null);
	}

	public function supprimerPanier()
	{
		// on recupere l'eventuel panier sinon on l'instancie à null
		$panier = $this->getPanier();

		// on supprime le panier s'il existe
		if (null !== $panier) {
			$this->session->remove('panier');
		}
	}

	public function ajouterLignePanier($ligne)
	{
		// s'il n'y a pas de panier en cours, on l'initialise
		if (!$this->session->has('panier')) {
			$this->initialiserPanier();
		}

		// on alias le panier
		$panier = $this->session->get('panier');

		if (array_key_exists($ligne['produit'], $panier)) {
			$quantite = $panier[$ligne['produit']];
			$panier[$ligne['produit']] = $ligne['quantite'] + $quantite;
		} else {
			$panier[$ligne['produit']] = $ligne['quantite'];
		}

		// on enregistre en session
		$this->session->set('panier', $panier);

	}

	public function modifierLignePanier()
	{
		die("@TODO");
	}

	public function supprimerLignePanier($id)
	{
		$panier = $this->getPanier();
		unset($panier[$id]);
		$this->session->set('panier', $panier);
	}
// endregion PANIER ///////////////////////////////////////////////////////////////////////////////////////////////////////////////


// region COMMANDE ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * @param User $monUser
	 * @return int
	 */
	public function creerCommande(User $monUser)
	{
		// controle /////////////////////////////////////////////////////////////////////////////////////////////////
		if (is_null($this->getPanier())) {
			throw new NotFoundHttpException("Le panier n'existe pas !");
		}
		if (is_null($monUser)) {
			throw new NotFoundHttpException("Le membre n'existe pas !");
		}

		// INITIALISATION ///////////////////////////////////////////////////////////////////////////////////////////
		/* @var $monUser User */
		// on recupere le statut
		// statut de commande = #1- en attente de paiement (par chèque)
		$monStatut = $this->em->getRepository(Statut::class)->findOneBy(['slug' => "en-attente-de-paiement"]);
		// on recupere le panier
		$panier = $this->getPanier();

		// les differentes variables
		$totalPayeTtc = 0;
		$totalPayeHt = 0;
		$totalPayeReel = 0;
		$totalProduitsTtc = 0;
		$totalProduitsHt = 0;
		$totalExpeditionTtc = 0;
		$totalExpeditionHt = 0;

		// on cree la commande //////////////////////////////////////////////////////////////////////////////////////
		$maCommande = new Commande();
		$maCommande->setUser($monUser);
		$maCommande->setAdresseLivraison($monUser->getAdresse());
		$maCommande->setAdresseFacturation($monUser->getAdresse());
		$maCommande->setStatut($monStatut);
		$maCommande->setReference($this->glService->genererChaineAleatoire(12, "ABCDEFGHJKLMNPQRSTUVWXYZ23456789"));
		$maCommande->setCle($this->glService->genererChaineAleatoire(32, "0123456789abcdefghijklmnopqrstuvwxyz"));
		$maCommande->setPaiement(Paiement::PAIEMENT_CB);
		$this->em->persist($maCommande);

		// on cree les lignes de commandes //////////////////////////////////////////////////////////////////////////
		foreach ($panier as $id => $quantite) {
			$monProduit = $this->em->getRepository(Wine::class)->find($id );
			if( !$monProduit ) {
				throw new NotFoundHttpException("Le produit n'existe pas !");
			}

			$maLigneCommande = new LigneCommande();
			$maLigneCommande->setProduitNom($monProduit->getName());
			$maLigneCommande->setProduitReference($monProduit->getReference());
			$maLigneCommande->setQuantite($quantite);
			$maLigneCommande->setPrixUnitaireTtc($monProduit->getPrice());
			$maLigneCommande->setPrixUnitaireHt($monProduit->getPrice() / (1 + $monProduit->getTaxe()->getTaux()/100));
			$maLigneCommande->setTotalPrixTtc($monProduit->getPrice() * $quantite);
			$maLigneCommande->setTotalPrixHt(($monProduit->getPrice() / (1 + $monProduit->getTaxe()->getTaux()/100)) * $quantite);
			$maLigneCommande->setTaxeNom($monProduit->getTaxe()->getNom());
			$maLigneCommande->setTaxeTaux($monProduit->getTaxe()->getTaux());
			$maLigneCommande->setWine($monProduit);

			// on persiste
			$this->em->persist($maLigneCommande);

			// on lie la ligne de commande et la commande.
			$maCommande->addLigneCommande($maLigneCommande);

			// on incremente les variables pour mettre à jour les infos de la commande
			$totalProduitsTtc += $maLigneCommande->getTotalPrixTtc();
			$totalProduitsHt += $maLigneCommande->getTotalPrixHt();
		}

		// on met a jour la commande ////////////////////////////////////////////////////////////////////////////////
		$totalPayeTtc =  $totalProduitsTtc + $totalExpeditionTtc;
		$totalPayeHt = $totalProduitsHt + $totalExpeditionHt;

		$maCommande->setTotalPayeTtc($totalPayeTtc);
		$maCommande->setTotalPayeHt($totalPayeHt);
		$maCommande->setTotalPayeReel($totalPayeReel);
		$maCommande->setTotalProduitsTtc($totalProduitsTtc);
		$maCommande->setTotalProduitsHt($totalProduitsHt);
		$maCommande->setTotalExpeditionTtc($totalExpeditionTtc);
		$maCommande->setTotalExpeditionHt($totalExpeditionHt);

		// on cree un historique commande ///////////////////////////////////////////////////////////////////////////
		$monHistorique = new Historique();
		$monHistorique->setUser($monUser);
		$monHistorique->setCommande($maCommande);
		$monHistorique->setStatut($monStatut);
		$monHistorique->setStatutNom($monStatut->getNom());
		$monHistorique->setCreatedAt(new \DateTime());
		$this->em->persist($monHistorique);

		// on enregistre en BDD /////////////////////////////////////////////////////////////////////////////////////
		$this->em->flush();

		// on retourne l'ID de la commande //////////////////////////////////////////////////////////////////////////
		return $maCommande->getId();
	}

	public function modifierCommande()
	{
		die("@TODO");
	}

	public function supprimerCommande()
	{
		// /!\ non permis en compat
		die("@TODO");
	}
// endregion COMMANDE /////////////////////////////////////////////////////////////////////////////////////////////////////////////


}