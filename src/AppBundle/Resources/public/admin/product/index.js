var adminProductIndex = function() {

    // AU CHARGEMENT ////////////////////////////////////////////////////
    var _load = function() {
        console.info("admin/product/index.js chargé.");
    };

    var _handleEvenements = function() {

        $("body").on("click", ".supprimer", function(event) {
            // Stop form from submitting normally
            event.preventDefault();
            // on enleve le precedent contenu du modal
            $(".modal-content").empty();
            // on recupere l'ID a supprimer
            var id = $(this).data('id');
            // on ajax
            $.ajax({
                type: "POST",
                url: Routing.generate('admin_wine_delete', { id: id }),
                data: {},
                success: function(data){
                    $("#modal_supprimer").find(".modal-content").html(data);
                }
            });
        });


    };


    var _handleTable = function() {

        var table = $('#table_articles');

        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            // "language": { "url": "{{ asset('assets/bo/global/plugins/datatables/locales/fr-FR.js') }}" },
            // Or you can use remote translation file
            "language": {
                url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/French.json'
            },
            "columns": [
                { "orderable": true},
                { "orderable": true},
                { "orderable": true},
                { "orderable": true},
                { "orderable": false}
            ],
            "order": [
                [0, 'desc']
            ],

            "lengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "Tous"]
            ],
            // set the initial value
            "pageLength": 10,

            // "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizontal scrollable datatable
            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });

        var tableWrapper = $('#table_articles_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

    };


    // RETURN ///////////////////////////////////////////////////////////
    return {
        init: function(options) {
            _load();
            _handleTable();
            _handleEvenements();
        }
    }
}();

jQuery(document).ready(function($) {
    // INIT
    var options = {};
    // var options = { 'vars': $("#js-vars").data("vars"), 'notifications': $('#js-notifications').data("notifications") };
    // var options = { 'tags': $("#js-tags").data("tags") };
    adminProductIndex.init(options);
});
