<?php
/**
 * Created by PhpStorm.
 * User: benjamin
 * Date: 20/09/2018
 * Time: 13:12
 */

namespace AppBundle\Form;

use AppBundle\Entity\Adresse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdresseType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('adresse', TextareaType::class)
			->add('codePostal', TextType::class)
			->add('ville', TextType::class)
			->add('pays', TextType::class)
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Adresse::class
		]);
	}
}