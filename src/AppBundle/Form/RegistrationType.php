<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class RegistrationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('gender',ChoiceType::class, [
	        	'required' => true,
		        'choices' => [
		        	'Mr' => 'mr',
			        'Mrs' => 'mrs',
			        'Undefined' => 'undefined'
		        ]])
            ->add('lastname',TextType::class)
            ->add('firstname',TextType::class)
	        ->add('adresse', AdresseType::class)
        ;
    }

    public function getParent(){
            return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }
}
