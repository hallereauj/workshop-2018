<?php

namespace AppBundle\Form;

use AppBundle\Entity\Wine;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProductType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name')
			->add('description', CKEditorType::class)
			->add('variete', ChoiceType::class, [
				'required'=>true,
				'choices'=>[
					'Rosé' => 'rose',
					'Blanc' => 'blanc',
					'Rouge' => 'rouge',
					'Effervescent' => 'effervescent'
				]
			])
			->add('price')
			->add('origin')
			// ->add('thumbnail', FileType::class)
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Wine::class
		]);
	}
}
