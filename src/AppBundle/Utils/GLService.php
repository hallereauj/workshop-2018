<?php

namespace AppBundle\Utils;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Templating\EngineInterface;
use Twig\Environment;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class GLService {

	private $em;
	private $mailer;
	private $twig;
	private $templating;
	private $authChecker;
	private $logger;

	/*
	 * Constructor
	 *
	 * @param EntityManagerInterface $em
	 * @param \Swift_Mailer $mailer
	 * @param Environment $twig
	 * @param Html2pdfFactory $html2pdf
	 */
	public function __construct(EntityManagerInterface $em, \Swift_Mailer $mailer, Environment $twig, EngineInterface $templating, AuthorizationCheckerInterface $authChecker, LoggerInterface $logger)
	{
		$this->em = $em;
		$this->mailer = $mailer;
		$this->twig = $twig;
		$this->templating = $templating;
		$this->authChecker = $authChecker;
		$this->logger = $logger;
	}


	// MIME TYPES ////////////////////////////////////////////////////////////////////
	public static $mimeTypesImage = [
		'image/png',
		'image/jpeg',
		'image/jpeg',
		'image/jpeg',
		'image/gif',
		'image/bmp',
		'image/vnd.microsoft.icon',
		'image/tiff',
		'image/tiff',
		'image/svg+xml',
		'image/svg+xml'
	];

	/**
	 * @param array $from
	 * @param array $to
	 * @param array $bcc
	 * @param string $subject
	 * @param string $template
	 * @param array $params
	 * @return int|string
	 */
	public function sendEmail(array $from=[], array $to=[], array $bcc=[], string $subject=null, string $template, array $params=[])
	{
		// on essaie d'envoyer le mail
		try {
			$email = \Swift_Message::newInstance()
				->setContentType("text/html")
				->setFrom($from)
				->setTo($to)
				->setSubject($subject)

				->setBody($this->twig->render('output/email/'.$template.'.html.twig',
					['params' => $params]))
				// ->addPart($this->twig->render($bodyTXT, array('params' => $params)), 'text/plain');
			;
			// $headers = $email->getHeaders();
			// $headers->addTextHeader('X-Mailer', 'PHP/'.phpversion());
			if (!empty($bcc)) {
				$email->setBcc($bcc);
			}

			// On envoie l'email
			return $this->mailer->send($email);

		} catch (\Exception $e) {
			$this->logger->critical($e->getMessage());
			return "Erreur : " . $e->getMessage() . "\n";
		}

	}

	/**
	 * @param string $media
	 * @param int $id
	 * @param string $filename
	 * @param string $role
	 * @return BinaryFileResponse|JsonResponse
	 */
	public function downloadFile(string $media, int $id, string $filename, string $role)
	{
		try {
			$monMedia = $this->em->getRepository('AppBundle:Media\\' . $media )->findOneBy(['id'=>$id, 'statut'=>true]);

			if (!$monMedia) {
				$aResponse = [
					'status' => 0,
					'message' => 'Le ficher n\'existe pas.'
				];
				$response = new JsonResponse($aResponse, 200);
				return $response;
			}


			$filepath = $monMedia->getAbsolutePath();
			// $filepath = $monMedia->getWebPath();

			// $basepath peut etre public (typically inside web/) or "internal"
			// $basepath = $this->container->getParameter('kernel.root_dir').'/../src/SKC/EspaceMembreBundle/uploads/doc';
			// $filepath = $basepath.'/'.$filename;
			// on check si le fichier existe
			$fs = new FileSystem();
			if (!$fs->exists($filepath)) {
				throw new NotFoundHttpException();
			}

			// if ($monMedia->getSlug()) {
			// $displayName = $monMedia->getSlug() . "." . $monMedia->getExtension();
			// } else {
			$displayName = $monMedia->getNom();
			// }

			// $response->headers->set ('Content-Type', 'text/plain');
			$response = new BinaryFileResponse($filepath);
			$response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $displayName);
			return $response;

		} catch ( \Exception $e ) {
			$aResponse = [
				'status' => 0,
				'message' => 'Erreur de téléchargement'
			];
			return new JsonResponse($aResponse, 400);
		}
	}


	/**
	 * @param $longueur
	 * @return string
	 */
	public function genererMotDePasse($longueur)
	{
		$mins = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
		$majs = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
		$specs = ['%', '_'];
		$mdp = '';
		for ($i = 1; $i <= $longueur; $i++) {
			// on génère un type aléatoire
			$type = rand(0, 1);
			switch ($type) {
				default:
				case '0':
					$caractere = rand(2, 9);
					$mdp .= $caractere;
					break;
				// case '1':
				//     $nbre_aleatoire = rand(0, 22);
				//     $mdp .= $mins[$nbre_aleatoire];
				//     break;
				case '1':
					$nbre_aleatoire = rand(0, 22);
					$mdp .= $majs[$nbre_aleatoire];
					break;
			}
		}
		return $mdp;
	}

	/**
	 * @param $nombreDeCaracteresAGenerer
	 * @param $chaineDeCaracteresautorises
	 * @return string
	 */
	public function genererChaineAleatoire($nombreDeCaracteresAGenerer, $chaineDeCaracteresautorises) : string
	{
		$nombreDeCaracteres = strlen($chaineDeCaracteresautorises) - 1;
		$chaineDeCaracteresGeneree = '';
		for($i = 0; $i < $nombreDeCaracteresAGenerer; $i++)
		{
			$pos = mt_rand( 0, $nombreDeCaracteres );
			$car = $chaineDeCaracteresautorises[$pos];
			$chaineDeCaracteresGeneree .= $car;
		}

		return $chaineDeCaracteresGeneree;
	}

}