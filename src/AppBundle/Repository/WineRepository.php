<?php

namespace AppBundle\Repository;

/**
 * WineRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class WineRepository extends \Doctrine\ORM\EntityRepository
{
    public function getWineByArguments($name,$minPrice,$maxPrice,$variete){

        $qb = $this->createQueryBuilder('p');

		if (!is_null($name)) {
			$qb->andWhere('p.name LIKE :name')->setParameter('name', '%'.$name.'%');
		}
        if (!is_null($minPrice)) {
			$qb->andWhere('p.price >= :min_price')->setParameter('min_price', $minPrice);
		}
        if (!is_null($maxPrice)) {
            $qb->andWhere('p.price <= :max_price')->setParameter('max_price', $maxPrice);
        }
        if (!is_null($variete)) {
            $qb->andWhere('p.variete LIKE :variete')->setParameter('variete', '%'.$variete.'%');
        }

		$qb->addOrderBy('p.createdAt', 'DESC');
		return $qb->getQuery()->getResult();
    }

    public function getSuggestions($id, $variete)
    {
    	$qb = $this->createQueryBuilder('p');

	    $qb->andWhere('p.id != :id')->setParameter('id', $id);
	    $qb->andWhere('p.variete LIKE :variete')->setParameter('variete', '%'.$variete.'%');
	    $qb->setMaxResults('4');

	    return $qb->getQuery()->getResult();
    }
}
