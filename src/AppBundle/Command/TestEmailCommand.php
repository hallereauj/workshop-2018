<?php

namespace AppBundle\Command;


use AppBundle\Entity\Boutique\Commande;
use AppBundle\Utils\GLService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestEmailCommand extends ContainerAwareCommand
{
	private $glService;
	private $em;

	public function __construct(GLService $glService, EntityManagerInterface $em)
	{
		$this->glService = $glService;
		$this->em = $em;
		parent::__construct();
	}

	public function configure()
	{
		$this
			->setName("app:test-email")
			->setDescription("Envoie un email de test")
			->setHelp("Envoie un email de test");
	}

	public function execute(InputInterface $input, OutputInterface $output)
	{
		$maCommande = $this->em->getRepository(Commande::class)->find(7);

		$from = ["benjamin.demonchy@gmail.com" => "Benjamin"];
		$to = ["benjamin.demonchy@imie.fr" => "Benjamin"];
		$bcc = [];
		$sujet = "Confirmation de commande";
		$template = "confirmation_commande";
		$params = [
			"maCommande" => $maCommande
		];

		$this->glService->sendEmail($from, $to, $bcc, $sujet, $template, $params);


	}

}