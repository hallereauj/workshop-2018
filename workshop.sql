-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:8889
-- Généré le :  ven. 21 sep. 2018 à 14:19
-- Version du serveur :  5.6.38
-- Version de PHP :  7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `workshop`
--

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

CREATE TABLE `adresse` (
  `id` int(11) NOT NULL,
  `adresse` longtext COLLATE utf8_unicode_ci NOT NULL,
  `code_postal` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `ville` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `pays` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `adresse`
--

INSERT INTO `adresse` (`id`, `adresse`, `code_postal`, `ville`, `pays`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '131 boulevard st michel', '49000', 'Angers', 'FRANCE', '2018-09-20 13:25:45', NULL, NULL),
(2, '131 boulevard st michel', '49100', 'Angers', 'FRANCE', '2018-09-20 13:28:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `btq_commande`
--

CREATE TABLE `btq_commande` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `adresse_livraison_id` int(11) NOT NULL,
  `adresse_facturation_id` int(11) NOT NULL,
  `statut_id` int(11) NOT NULL,
  `reponse_acquereur_id` int(11) DEFAULT NULL,
  `reference` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cle` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `paiement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentaires` longtext COLLATE utf8_unicode_ci,
  `numero_expedition` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_reductions_ttc` decimal(17,2) NOT NULL,
  `total_reductions_ht` decimal(17,2) NOT NULL,
  `total_paye_ttc` decimal(17,2) NOT NULL,
  `total_paye_ht` decimal(17,2) NOT NULL,
  `total_paye_reel` decimal(17,2) NOT NULL,
  `total_produits_ttc` decimal(17,2) NOT NULL,
  `total_produits_ht` decimal(17,2) NOT NULL,
  `total_expedition_ttc` decimal(17,2) NOT NULL,
  `total_expedition_ht` decimal(17,2) NOT NULL,
  `numero_facture` int(11) DEFAULT NULL,
  `numero_livraison` int(11) DEFAULT NULL,
  `facture_created_at` datetime DEFAULT NULL,
  `livraison_at` datetime DEFAULT NULL,
  `is_valide` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `btq_commande`
--

INSERT INTO `btq_commande` (`id`, `user_id`, `adresse_livraison_id`, `adresse_facturation_id`, `statut_id`, `reponse_acquereur_id`, `reference`, `cle`, `paiement`, `commentaires`, `numero_expedition`, `total_reductions_ttc`, `total_reductions_ht`, `total_paye_ttc`, `total_paye_ht`, `total_paye_reel`, `total_produits_ttc`, `total_produits_ht`, `total_expedition_ttc`, `total_expedition_ht`, `numero_facture`, `numero_livraison`, `facture_created_at`, `livraison_at`, `is_valide`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, NULL, 'T53FEDVRRRHH', '5vwwuxjjclgnmeee51rmhdic967ta80o', 'CB', NULL, NULL, '0.00', '0.00', '120.00', '100.00', '0.00', '120.00', '100.00', '0.00', '0.00', NULL, NULL, NULL, NULL, 0, '2018-09-21 13:48:16', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `btq_facture`
--

CREATE TABLE `btq_facture` (
  `id` int(11) NOT NULL,
  `commande_id` int(11) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `numero_livraison` int(11) DEFAULT NULL,
  `livraison_at` datetime DEFAULT NULL,
  `total_reductions_ttc` decimal(17,2) NOT NULL,
  `total_reductions_ht` decimal(17,2) NOT NULL,
  `total_paye_ttc` decimal(17,2) NOT NULL,
  `total_paye_ht` decimal(17,2) NOT NULL,
  `total_produits_ttc` decimal(17,2) NOT NULL,
  `total_produits_ht` decimal(17,2) NOT NULL,
  `total_expedition_ttc` decimal(17,2) NOT NULL,
  `total_expedition_ht` decimal(17,2) NOT NULL,
  `commentaires` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `btq_historique`
--

CREATE TABLE `btq_historique` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `commande_id` int(11) NOT NULL,
  `statut_id` int(11) NOT NULL,
  `statut_nom` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `btq_historique`
--

INSERT INTO `btq_historique` (`id`, `user_id`, `commande_id`, `statut_id`, `statut_nom`, `created_at`) VALUES
(1, 1, 1, 1, 'En attente du paiement', '2018-09-21 13:48:16');

-- --------------------------------------------------------

--
-- Structure de la table `btq_lignecommande`
--

CREATE TABLE `btq_lignecommande` (
  `id` int(11) NOT NULL,
  `commande_id` int(11) NOT NULL,
  `wine_id` int(11) NOT NULL,
  `facture_id` int(11) DEFAULT NULL,
  `produit_nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produit_reference` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantite` int(11) NOT NULL,
  `prix_unitaire_ttc` decimal(17,2) NOT NULL,
  `prix_unitaire_ht` decimal(17,2) NOT NULL,
  `total_prix_ttc` decimal(17,2) NOT NULL,
  `total_prix_ht` decimal(17,2) NOT NULL,
  `reduction_pourcent` decimal(10,2) NOT NULL,
  `reduction_montant_ttc` decimal(17,2) NOT NULL,
  `reduction_montant_ht` decimal(17,2) NOT NULL,
  `taxe_nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `taxe_taux` decimal(10,3) NOT NULL,
  `ecotaxe` decimal(17,2) NOT NULL,
  `total_expedition_ttc` decimal(17,2) NOT NULL,
  `total_expedition_ht` decimal(17,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `btq_lignecommande`
--

INSERT INTO `btq_lignecommande` (`id`, `commande_id`, `wine_id`, `facture_id`, `produit_nom`, `produit_reference`, `quantite`, `prix_unitaire_ttc`, `prix_unitaire_ht`, `total_prix_ttc`, `total_prix_ht`, `reduction_pourcent`, `reduction_montant_ttc`, `reduction_montant_ht`, `taxe_nom`, `taxe_taux`, `ecotaxe`, `total_expedition_ttc`, `total_expedition_ht`) VALUES
(1, 1, 1, NULL, 'Ajaccio', 'JYTKP', 2, '60.00', '50.00', '120.00', '100.00', '0.00', '0.00', '0.00', 'TVA FR Taux standard 20%', '20.000', '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Structure de la table `btq_reponseacquereur`
--

CREATE TABLE `btq_reponseacquereur` (
  `id` int(11) NOT NULL,
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `signification` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `btq_statut`
--

CREATE TABLE `btq_statut` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice` tinyint(1) NOT NULL,
  `send_email` tinyint(1) NOT NULL,
  `color` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `unremovable` tinyint(1) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `logable` tinyint(1) NOT NULL,
  `delivery` tinyint(1) NOT NULL,
  `shipped` tinyint(1) NOT NULL,
  `paid` tinyint(1) NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `btq_statut`
--

INSERT INTO `btq_statut` (`id`, `nom`, `slug`, `template`, `invoice`, `send_email`, `color`, `unremovable`, `hidden`, `logable`, `delivery`, `shipped`, `paid`, `deleted_at`) VALUES
(1, 'En attente du paiement', 'en-attente-de-paiement', 'cheque', 0, 0, 'RoyalBlue', 1, 1, 0, 0, 0, 0, NULL),
(2, 'Paiement accepté', 'paiement-accepte', 'paiement', 1, 1, 'LimeGreen', 1, 0, 1, 0, 0, 1, NULL),
(3, 'En cours de préparation', 'en-cours-de-preparation', 'preparation', 1, 1, 'DarkOrange', 1, 1, 1, 1, 0, 1, NULL),
(4, 'En cours d\'expédition', 'en-cours-d-expedition', 'shipped', 1, 1, 'BlueViolet', 1, 1, 1, 1, 1, 1, NULL),
(5, 'Livré partiellement', 'livre-partiellement', NULL, 1, 1, '#108510', 1, 1, 1, 1, 1, 1, NULL),
(6, 'Livré', 'livre', NULL, 0, 0, '#B752B0', 1, 1, 0, 0, 0, 0, NULL),
(7, 'Annulé', 'annule', 'cancel', 1, 1, '#ec2e15', 1, 1, 0, 0, 0, 0, NULL),
(8, 'Remboursé', 'rembourse', NULL, 0, 1, 'Red', 1, 1, 0, 0, 0, 0, NULL),
(12, 'En attente de règlement facture', 'en-attente-de-reglement-facture', NULL, 0, 1, 'RoyalBlue', 1, 1, 0, 1, 1, 0, NULL),
(13, 'Règlement facture accepté', 'reglement-facture-accepte', NULL, 1, 1, 'LimeGreen', 1, 1, 0, 1, 1, 1, NULL),
(99, 'Erreur de paiement', 'erreur-de-paiement', 'payment_error', 0, 1, '#8f0621', 1, 1, 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `btq_taxe`
--

CREATE TABLE `btq_taxe` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `taux` decimal(10,2) NOT NULL,
  `rang` int(11) NOT NULL,
  `statut` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `btq_taxe`
--

INSERT INTO `btq_taxe` (`id`, `nom`, `reference`, `taux`, `rang`, `statut`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'TVA FR Taux standard 20%', 'tva-fr-taux-standard-20', '20.00', 1, 1, '2015-09-21 00:00:00', NULL, NULL),
(2, 'TVA FR Taux réduit 2.10%', 'tva-fr-taux-reduit-2-10', '2.10', 2, 1, '2016-02-03 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adresse_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `gender`, `lastname`, `firstname`, `adresse_id`) VALUES
(1, 'benjamin', 'benjamin', 'benjamin.demonchy@gmail.com', 'benjamin.demonchy@gmail.com', 1, NULL, '$2y$13$IGTGerqBPnZOeWriT7GMeeIE307rxASvKaP6//V62Lf73E1A.Yt3.', '2018-09-21 11:50:05', NULL, NULL, 'a:1:{i:0;s:15:\"ROLE_SUPERADMIN\";}', 'mr', 'de Monchy', 'Benjamin', 1),
(6, 'bdemonchy', 'bdemonchy', 'benjamin.demonchy@imie.fr', 'benjamin.demonchy@imie.fr', 1, NULL, '$2y$13$2Uy56txHLDQDL9oabkj8h.7XIX463jsIA8s4ESwTKuZ57ZajJL4Em', '2018-09-20 13:28:36', NULL, NULL, 'a:0:{}', 'mr', 'de Monchy', 'Benjamin', 2);

-- --------------------------------------------------------

--
-- Structure de la table `wine`
--

CREATE TABLE `wine` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `origin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `taxe_id` int(11) DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `variete` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `wine`
--

INSERT INTO `wine` (`id`, `name`, `description`, `price`, `thumbnail`, `origin`, `created_at`, `deleted_at`, `taxe_id`, `reference`, `variete`) VALUES
(1, 'Ajaccio', '<p>Lorem ipsum</p>', 60, '826ba34caf931268185e4cbbb7ade3f6.jpeg', 'Corse', '2018-09-21 11:17:44', NULL, 1, 'JYTKP', 'rose'),
(2, 'Anjou', '<p>Lorem ipsum</p>', 90, '98ead05e920e057d9b06a18278d501db.jpeg', 'Val de Loire', '2018-09-21 11:21:02', NULL, 1, 'G29ST', 'effervescent'),
(3, 'Barsac', '<p>Lorem ipsum</p>', 20, 'd8a62dacc8e411980fadae2d253390d0.jpeg', 'Corse', '2018-09-21 11:22:14', NULL, 1, 'KYWD8', 'rose'),
(4, 'Beaujolais', '<p>Lorem ipsum</p>', 20, '0e6db3e5c3239a18788610a2124bcffd.jpeg', 'Bordeaux', '2018-09-21 11:23:06', NULL, 1, 'MMG6P', 'rouge'),
(5, 'Chenas', '<p>Lorem ipsum</p>', 80, 'f8340d88a8dc6e8a71a57c121fc4f890.jpeg', 'Corse', '2018-09-21 11:25:25', NULL, 1, 'HCEJ8', 'rose'),
(6, 'CHIROUBLES', '<p>Lorem ipsum</p>', 80, '89198bf768526f1c198a2369652f73c4.jpeg', 'Corse', '2018-09-21 11:26:05', NULL, 1, 'P6CN8', 'rose'),
(7, 'COTE DE BROUILLY', '<p>Lorem ipsum</p>', 100, 'eba591f40835bd7aca644311a9a1b04a.jpeg', 'Corse', '2018-09-21 11:26:43', NULL, 1, 'DDDJ4', 'rose');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `adresse`
--
ALTER TABLE `adresse`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `btq_commande`
--
ALTER TABLE `btq_commande`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_4EEF0C9CA76ED395` (`user_id`),
  ADD KEY `IDX_4EEF0C9CBE2F0A35` (`adresse_livraison_id`),
  ADD KEY `IDX_4EEF0C9C5BBD1224` (`adresse_facturation_id`),
  ADD KEY `IDX_4EEF0C9CF6203804` (`statut_id`),
  ADD KEY `IDX_4EEF0C9C9CB09` (`reponse_acquereur_id`);

--
-- Index pour la table `btq_facture`
--
ALTER TABLE `btq_facture`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_94A9366582EA2E54` (`commande_id`);

--
-- Index pour la table `btq_historique`
--
ALTER TABLE `btq_historique`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9CD97DB2A76ED395` (`user_id`),
  ADD KEY `IDX_9CD97DB282EA2E54` (`commande_id`),
  ADD KEY `IDX_9CD97DB2F6203804` (`statut_id`);

--
-- Index pour la table `btq_lignecommande`
--
ALTER TABLE `btq_lignecommande`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_2116887E82EA2E54` (`commande_id`),
  ADD KEY `IDX_2116887E28A2BD76` (`wine_id`);

--
-- Index pour la table `btq_reponseacquereur`
--
ALTER TABLE `btq_reponseacquereur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `btq_statut`
--
ALTER TABLE `btq_statut`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_7A1677AE989D9B62` (`slug`);

--
-- Index pour la table `btq_taxe`
--
ALTER TABLE `btq_taxe`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`),
  ADD UNIQUE KEY `UNIQ_8D93D6494DE7DC5C` (`adresse_id`);

--
-- Index pour la table `wine`
--
ALTER TABLE `wine`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_560C64681AB947A4` (`taxe_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `adresse`
--
ALTER TABLE `adresse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `btq_commande`
--
ALTER TABLE `btq_commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `btq_facture`
--
ALTER TABLE `btq_facture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `btq_historique`
--
ALTER TABLE `btq_historique`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `btq_lignecommande`
--
ALTER TABLE `btq_lignecommande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `btq_reponseacquereur`
--
ALTER TABLE `btq_reponseacquereur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `btq_statut`
--
ALTER TABLE `btq_statut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT pour la table `btq_taxe`
--
ALTER TABLE `btq_taxe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `wine`
--
ALTER TABLE `wine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `btq_commande`
--
ALTER TABLE `btq_commande`
  ADD CONSTRAINT `FK_4EEF0C9C5BBD1224` FOREIGN KEY (`adresse_facturation_id`) REFERENCES `adresse` (`id`),
  ADD CONSTRAINT `FK_4EEF0C9C9CB09` FOREIGN KEY (`reponse_acquereur_id`) REFERENCES `btq_reponseacquereur` (`id`),
  ADD CONSTRAINT `FK_4EEF0C9CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_4EEF0C9CBE2F0A35` FOREIGN KEY (`adresse_livraison_id`) REFERENCES `adresse` (`id`),
  ADD CONSTRAINT `FK_4EEF0C9CF6203804` FOREIGN KEY (`statut_id`) REFERENCES `btq_statut` (`id`);

--
-- Contraintes pour la table `btq_facture`
--
ALTER TABLE `btq_facture`
  ADD CONSTRAINT `FK_94A9366582EA2E54` FOREIGN KEY (`commande_id`) REFERENCES `btq_commande` (`id`);

--
-- Contraintes pour la table `btq_historique`
--
ALTER TABLE `btq_historique`
  ADD CONSTRAINT `FK_9CD97DB282EA2E54` FOREIGN KEY (`commande_id`) REFERENCES `btq_commande` (`id`),
  ADD CONSTRAINT `FK_9CD97DB2A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_9CD97DB2F6203804` FOREIGN KEY (`statut_id`) REFERENCES `btq_statut` (`id`);

--
-- Contraintes pour la table `btq_lignecommande`
--
ALTER TABLE `btq_lignecommande`
  ADD CONSTRAINT `FK_2116887E28A2BD76` FOREIGN KEY (`wine_id`) REFERENCES `wine` (`id`),
  ADD CONSTRAINT `FK_2116887E82EA2E54` FOREIGN KEY (`commande_id`) REFERENCES `btq_commande` (`id`);

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D6494DE7DC5C` FOREIGN KEY (`adresse_id`) REFERENCES `adresse` (`id`);

--
-- Contraintes pour la table `wine`
--
ALTER TABLE `wine`
  ADD CONSTRAINT `FK_560C64681AB947A4` FOREIGN KEY (`taxe_id`) REFERENCES `btq_taxe` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
